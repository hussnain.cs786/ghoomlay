package org.ninesteps.ghoomlay.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mikhaellopez.circularimageview.CircularImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main_list.*
import org.jetbrains.anko.toast
import org.ninesteps.ghoomlay.CitiesList
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.GL.Companion.u
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.activity.admin.AddCity
import org.ninesteps.ghoomlay.activity.admin.DriverRequest
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.City
import java.util.HashMap

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    override fun onSliderClick(slider: BaseSliderView?) {

    }

//    private var mListType = RecyclerViewAdapter.VIEW_TYPE_CITIES
//    private var mAdapter: RecyclerViewAdapter? = null
//    var mList: MutableList<Any> = ArrayList()

    internal lateinit var file_maps: HashMap<String, Int>

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        view_cities.setOnClickListener {
            startActivity(Intent(this,CitiesList::class.java))
        }

//        addcity.setOnClickListener {
//            startActivity(Intent(this@MainActivity, AddCity::class.java))
//        }
//
//        if (GL.isSignedIn()) {
//            if (u!!.userType.equals("admin")) {
//                addcity.visibility = View.VISIBLE
//            } else {
//                addcity.visibility = View.GONE
//            }
//        }else{
//            addcity.visibility = View.GONE
//        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val v = nav_view.getHeaderView(0)

        val name: TextView = v.findViewById(R.id.header_name)
        val email: TextView = v.findViewById(R.id.header_email)
        val imageView: CircularImageView = v.findViewById(R.id.header_image)

        if (GL.isSignedIn()) {
            name.text = u!!.name
            email.text = u!!.email
        }

        //String to Bitmap
        try {
            val encodeByte = Base64.decode(u!!.thumbnail, Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
            imageView.setImageBitmap(bitmap)
        } catch (e: Exception) {
            e.message

        }



//        GL.getDatabaseRef().child("cities").addValueEventListener(object : ValueEventListener {
//            override fun onCancelled(p0: DatabaseError?) {
//
//            }
//
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                Log.e("CITIES", "OK")
//                mList.clear()
//                for (dataSnapshot1 in dataSnapshot.children) {
//
//                    val model = dataSnapshot1.getValue(City::class.java)!!
//                    mList.add(0, model)
//
//                }
//                mAdapter = RecyclerViewAdapter(mList, mListType, this@MainActivity)
//                city_list.adapter = mAdapter
//            }
//        })


        val nav_Menu = nav_view.menu

        if (GL.isSignedIn()) {
            if (u!!.userType.equals("admin")) {
                nav_Menu.findItem(R.id.nav_drivers).isVisible = true
                nav_Menu.findItem(R.id.nav_drivers_request).isVisible = true
            } else {
                nav_Menu.findItem(R.id.nav_drivers).isVisible = false
                nav_Menu.findItem(R.id.nav_drivers_request).isVisible = false
            }
        } else {
            nav_Menu.findItem(R.id.nav_drivers).isVisible = false
            nav_Menu.findItem(R.id.nav_drivers_request).isVisible = false
        }

        navView.setNavigationItemSelectedListener(this)


        val slider_title = arrayOf("Slide one", "Slide two", "Slide three", "Slide four", "Slide five",
            "Slide six", "Slide seven", "Slide eight", "Slide nine")
        val slider_thumbnail = intArrayOf(
            R.drawable.imagesone,
            R.drawable.imagestwo,
            R.drawable.imagesthree,
            R.drawable.imagesfour,
            R.drawable.imagesfive,
            R.drawable.imagessix,
            R.drawable.imagesseven,
            R.drawable.imageseight,
            R.drawable.imagesnine
        )

        file_maps = HashMap()

        for (i in 0 until slider_title.size) {
            file_maps.put(slider_title[i], slider_thumbnail[i])
        }

        for (name in file_maps.keys) {
            val textSliderView = TextSliderView(this)
            // initialize a SliderLayout
            textSliderView
//                .description(name)
                .image(file_maps[name]!!)
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .setOnSliderClickListener(this)

            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                .putString("extra", name)

            slider.addSlider(textSliderView)
        }

        slider.setPresetTransformer(SliderLayout.Transformer.Accordion)
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider.setCustomAnimation(DescriptionAnimation())
        slider.setDuration(4000)
        slider.addOnPageChangeListener(this)
    }


    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
//        val menuItem = menu.findItem(R.id.action_search)
//        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView
//        searchView.queryHint = getString(R.string.search_cities)
//        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_chat -> {
                if (GL.isSignedIn()) {
                    startActivity(Intent(this@MainActivity, ChatList::class.java))
                } else {
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    // close this activity
//                    finish()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val action_map = menu!!.findItem(R.id.action_map)
        val action_chat = menu.findItem(R.id.action_chat)
        val action_search = menu.findItem(R.id.action_search)
        val action_add = menu.findItem(R.id.action_add)

        action_add.isVisible = false
        action_map.isVisible = false
        action_search.isVisible = false

        return super.onPrepareOptionsMenu(menu)
    }
/*

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        when (mListType) {
            RecyclerViewAdapter.VIEW_TYPE_CITIES -> {

                val newList = ArrayList<Any>()
                for (model in mList) {
                    model as City
                    val name = model.name!!.toLowerCase()
                    val province = model.province!!.toLowerCase()
                    if (name.contains(newText!!.toLowerCase())
                        or province.contains(newText.toLowerCase())
                    )
                        newList.add(model)
                }
                mAdapter!!.setFilter(newList)
            }
        }
        return true
    }
*/

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_drivers -> {
                val intent = Intent(this@MainActivity, AllDrivers::class.java)
                intent.putExtra(GL.DRIVER_STATUS, "all")
                startActivity(intent)
            }
            R.id.nav_drivers_request -> {
                startActivity(Intent(this@MainActivity, DriverRequest::class.java))
            }
            R.id.nav_profile -> {
                val intent = Intent(this@MainActivity, DriverDetail::class.java)
                intent.putExtra("TYPE", "profile")
                startActivity(intent)
            }
            R.id.nav_about -> {
                startActivity(Intent(this@MainActivity, AboutActivity::class.java))
            }
            R.id.nav_register -> {
                if (GL.isSignedIn()) {
                    toast("You are already register")
                } else {
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    // close this activity
//                    finish()
                }
            }
            R.id.nav_logout -> {
                if (GL.isSignedIn()) {
                    GL.getAuth().signOut()
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    finish()
                }else{
                    toast("You're already logged out!")
                }
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}
