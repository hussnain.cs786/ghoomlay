package org.ninesteps.ghoomlay.activity.driver

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_ref_city.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import java.util.*

class RefCityActivity : AppCompatActivity() {

    var ref1: String? = null
    var ref2: String? = null
    var ref3: String? = null
    var ref4: String? = null
    var ref5: String? = null

    var mProgressDialog: ProgressDialog? = null

    internal lateinit var arrayAdapter: ArrayAdapter<*>
    internal lateinit var arrayAdapter2: ArrayAdapter<*>
    internal lateinit var arrayAdapter3: ArrayAdapter<*>
    internal lateinit var arrayAdapter4: ArrayAdapter<*>
    internal lateinit var arrayAdapter5: ArrayAdapter<*>

    internal var filter = arrayOf(
        "Abbottabad",
        "Attok",
        "Bagh",
        "Bahawalnagar",
        "Bahawalpur",
        "Bannu",
        "Biosphere reserves of Pakistan",
        "Bolan Pass",
        "Central Karakoram National Park",
        "Chakwal",
        "Changa Manga",
        "Chiniot",
        "Chitral",
        "Dera Ghazi Khan",
        "Fairy Meadows",
        "Faisalabad",
        "Gilgat",
        "Great Rann of Kutch",
        "Gujar Khan",
        "Gujranwala",
        "Gujrat",
        "Gulmit",
        "Gwadar",
        "Hafizabad",
        "Hangu",
        "Hyderabad",
        "Islamabad",
        "Jhang",
        "Jhelum",
        "kalasha Valleys",
        "Karachi",
        "Karimabad",
        "Kasur",
        "Keenjhar Lake",
        "Kharmang",
        "Khewra Salt Mine",
        "Kohat",
        "Lahore",
        "Landi Kotal",
        "Larkana",
        "Lulusar",
        "Madyan",
        "Mardan",
        "Mehrgah",
        "Mirpur",
        "Mirpur Khas",
        "Mithi",
        "Mohenjo-Daro",
        "Multan",
        "Murree",
        "Muzaffarabad",
        "Muzaffargarh",
        "Nagarparkar",
        "Naran",
        "Ormara",
        "Patriata",
        "Peshawar",
        "Quetta",
        "Rahimyar Khan",
        "Raja Village",
        "Rawalpindi",
        "Sahiwal",
        "Saiful Muluk National Park",
        "Samundri",
        "Sargodha",
        "Sehwan",
        "Seri Bahlol",
        "Sheikhan",
        "Sheikhupura",
        "Sialkot",
        "Skardu",
        "Soon Valley",
        "Sukkar",
        "Swat",
        "Tando Allahyar",
        "Taxila",
        "Thatta",
        "Tulamba",
        "Turbat",
        "Umerkot",
        "Wagah",
        "Ziarat"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ref_city)

        title = "Add References"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mProgressDialog = ProgressDialog(this)

        ref1_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ref1 = adapterView!!.getItemAtPosition(position).toString()
            }

        }
        ref2_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ref2 = adapterView!!.getItemAtPosition(position).toString()
            }

        }
        ref3_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ref3 = adapterView!!.getItemAtPosition(position).toString()
            }

        }
        ref4_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ref4 = adapterView!!.getItemAtPosition(position).toString()
            }

        }
        ref5_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
                ref5 = adapterView!!.getItemAtPosition(position).toString()
            }

        }

        arrayAdapter = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter.setDropDownViewResource(R.layout.spinner_item)
        ref1_spinner.adapter = arrayAdapter
        arrayAdapter2 = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter2.setDropDownViewResource(R.layout.spinner_item)
        ref2_spinner.adapter = arrayAdapter2
        arrayAdapter3 = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter3.setDropDownViewResource(R.layout.spinner_item)
        ref3_spinner.adapter = arrayAdapter3
        arrayAdapter4 = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter4.setDropDownViewResource(R.layout.spinner_item)
        ref4_spinner.adapter = arrayAdapter4
        arrayAdapter5 = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter5.setDropDownViewResource(R.layout.spinner_item)
        ref5_spinner.adapter = arrayAdapter5

        add_ref.setOnClickListener {
            val ref = GL.getDatabaseRef().child("users").child(GL.getUserId())

//            val k = GL.getDatabaseRef().child("pending-drivers").push().key
//            val refer = GL.getDatabaseRef().child("pending-drivers").child(k)

            val map = HashMap<String, String>()

            map.put("uid", GL.u!!.uid!!)
            map.put("name", GL.u!!.name!!)
            map.put("email", GL.u!!.email!!)
            map.put("password", GL.u!!.password!!)
            map.put("phone", GL.u!!.phone!!)
            map.put("nic", GL.u!!.nic!!)
            map.put("thumbnail", GL.u!!.thumbnail!!)
            map.put("userType", "driver")

            map.put("brand", GL.u!!.brand!!)
            map.put("model", GL.u!!.model!!)
            map.put("year", GL.u!!.year!!)
            map.put("color", GL.u!!.color!!)
            map.put("interiorColor", GL.u!!.interiorColor!!)
            map.put("vehicleThumbnail", GL.u!!.vehicleThumbnail!!)

            map.put("licenseNumber", GL.u!!.licenseNumber!!)
            map.put("vehicleValidType", GL.u!!.vehicleValidType!!)
            map.put("issuedDate", GL.u!!.issuedDate!!)
            map.put("expiryDate", GL.u!!.expiryDate!!)
            map.put("licenseThumbnail", GL.u!!.licenseThumbnail!!)

            map.put("refOne", ref1.toString())
            map.put("refTwo", ref2.toString())
            map.put("refThree", ref3.toString())
            map.put("refFour", ref4.toString())
            map.put("refFive", ref5.toString())

            map.put("offerOne", GL.u!!.offerOne!!)
            map.put("offerTwo", GL.u!!.offerTwo!!)
            map.put("offerThree", GL.u!!.offerThree!!)

            mProgressDialog!!.setTitle("Processing")
            mProgressDialog!!.setMessage("Please wait...")
            mProgressDialog!!.setCanceledOnTouchOutside(false)
            mProgressDialog!!.show()

            ref.setValue(map).addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    mProgressDialog!!.dismiss()

                    GL.u!!.uid = GL.u!!.uid!!
                    GL.u!!.name = GL.u!!.name!!
                    GL.u!!.email = GL.u!!.email!!
                    GL.u!!.password = GL.u!!.password!!
                    GL.u!!.phone = GL.u!!.phone!!
                    GL.u!!.nic = GL.u!!.nic!!
                    GL.u!!.thumbnail = GL.u!!.thumbnail!!
                    GL.u!!.userType = "user"

                    GL.u!!.brand = GL.u!!.brand!!
                    GL.u!!.model = GL.u!!.model!!
                    GL.u!!.year = GL.u!!.year!!
                    GL.u!!.color = GL.u!!.color!!
                    GL.u!!.interiorColor = GL.u!!.interiorColor!!
                    GL.u!!.vehicleThumbnail = GL.u!!.vehicleThumbnail!!

                    GL.u!!.licenseNumber = GL.u!!.licenseNumber
                    GL.u!!.vehicleValidType = GL.u!!.vehicleValidType
                    GL.u!!.issuedDate = GL.u!!.issuedDate
                    GL.u!!.expiryDate = GL.u!!.expiryDate
                    GL.u!!.licenseThumbnail = GL.u!!.licenseThumbnail

                    GL.u!!.refOne = ref1.toString()
                    GL.u!!.refTwo = ref2.toString()
                    GL.u!!.refThree = ref3.toString()
                    GL.u!!.refFour = ref4.toString()
                    GL.u!!.refFive = ref5.toString()

                    GL.u!!.offerOne = GL.u!!.offerOne
                    GL.u!!.offerTwo = GL.u!!.offerTwo
                    GL.u!!.offerThree = GL.u!!.offerThree

                    val intent = Intent(this, AddOffers::class.java)
                    startActivity(intent)
                }
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
