package org.ninesteps.ghoomlay.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    var address = "Lahore"
    var longitude = 31.4826352
    var latitude = 74.054189

    var loc: ArrayList<String> = ArrayList()
    var lon: ArrayList<String> = ArrayList()
    var lat: ArrayList<String> = ArrayList()

    var maptype: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        maptype = intent.getStringExtra(GL.MAP_STATUS)
        if (maptype == "single") {
            address = intent.getStringExtra(GL.MAP_TITLE)
            longitude = intent.getDoubleExtra(GL.MAP_LONGITUDE, 74.054189)
            latitude = intent.getDoubleExtra(GL.MAP_LATITUDE, 31.4826352)
            Log.e("MAP DATA", address)
        } else if (maptype == "list") {
            loc = intent.getStringArrayListExtra("LOC")
            lon = intent.getStringArrayListExtra("LON")
            lat = intent.getStringArrayListExtra("LAT")
        }


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (maptype == "single") {
            // Add a marker in Sydney and move the camera
            val title = LatLng(latitude, longitude)
            mMap.addMarker(MarkerOptions().position(title).title("Marker in $address"))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(title,16.0f))
        } else if (maptype == "list") {
            for (i in 0 until loc.size) {
                Log.e("ADDRESS: ", "${loc[i]} , ${lon[i]} , ${lat[i]}")
                createMarker(lat[i].toDouble(), lon[i].toDouble(), loc[i], loc[i])
            }
        }
    }

    protected fun createMarker(
        latitude: Double,
        longitude: Double,
        title: String,
        snippet: String
    ){

        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(latitude, longitude))
//                .anchor(1.0f, 1.0f)
                .title(title)
                .snippet(snippet)
//                .icon(BitmapDescriptorFactory.fromResource(iconResID))
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude),12.0f))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
