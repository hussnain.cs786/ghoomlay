package org.ninesteps.ghoomlay.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_point_detail.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.Point

class PointDetail : AppCompatActivity() {

    var model: Point? = null
    var loc: ArrayList<String> = ArrayList()
    var lon: ArrayList<String> = ArrayList()
    var lat: ArrayList<String> = ArrayList()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_point_detail)

        model = intent.getSerializableExtra(GL.MODEL_POINTS) as Point

        title = model!!.name
        supportActionBar!!.subtitle = model!!.city
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        pd_name.text = model!!.name
        pd_time.text = "Hour: ${model!!.opentime} - ${model!!.closetime}"

        val bitmap = stringToBitMap(model!!.thumbnail.toString())
        pd_image.setImageBitmap(bitmap)

        pd_location.setOnClickListener {
            alert {
                title = model!!.name.toString()
                message = "Reference: ${model!!.refrence.toString()}"
                positiveButton("Map") {
                    val intent = Intent(this@PointDetail, MapsActivity::class.java)
                    intent.putExtra(GL.MAP_STATUS, "single")
                    intent.putExtra(GL.MAP_TITLE, model!!.location)
                    intent.putExtra(GL.MAP_LONGITUDE, model!!.longitude)
                    intent.putExtra(GL.MAP_LATITUDE, model!!.latitude)
                    startActivity(intent)
                }
                negativeButton("Cancel") {
                    toast("Canceled!")
                }
            }.show()

        }
        pd_call.setOnClickListener {
            if (model!!.phone != "default") {
                toast("phone number not available.")
            } else {
                alert {
                    title = model!!.name.toString()
                    message = "Phone: ${model!!.phone.toString()}"
                    positiveButton("Call") {
                        phoneCall()
                    }
                    negativeButton("Cancel") {
                        toast("Canceled!")
                    }
                }.show()
            }
        }
        pd_share.setOnClickListener {
            share()
        }

    }

    fun share() {
        //Get text from TextView and store in variable "s"
        val s = "Name: ${model!!.name.toString()}\n" +
                "Phone: ${model!!.phone.toString()}\n" +
                "Address: http://maps.google.com/maps?saddr=${model!!.latitude},${model!!.longitude}"
        //Intent to share the text
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, s);
        startActivity(Intent.createChooser(shareIntent, "Share via"))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }

    fun phoneCall() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.CALL_PHONE
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    42
                )
            }
        } else {
            // Permission has already been granted
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + model!!.phone)))
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + model!!.phone)))
            } else {
                // permission denied, boo! Disable the
                // functionality
            }
            return
        }
    }
}
