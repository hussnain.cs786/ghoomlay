package org.ninesteps.ghoomlay.activity

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_all_drivers_list.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.Driver

class AllDrivers : AppCompatActivity(), SearchView.OnQueryTextListener {

    var city: String? = null

    private var mListType = RecyclerViewAdapter.VIEW_TYPE_DRIVERS
    private var mAdapter: RecyclerViewAdapter? = null
    var mList: MutableList<Any> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_drivers_list)

        val driverStatus = intent.getStringExtra(GL.DRIVER_STATUS)
        if (driverStatus == "all") {
            title = "Agents"
        } else if (driverStatus == "recommended") {
            title = "Recommended Agents"
            city = intent.getStringExtra(GL.DRIVER_CITY)
            supportActionBar!!.subtitle = city
        }


        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        GL.getDatabaseRef().child("all-drivers").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("DRIVERS", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {
                    if (driverStatus == "all") {
                        val model = dataSnapshot1.getValue(Driver::class.java)!!
                        mList.add(0, model)
                    } else if (driverStatus == "recommended") {
                        val model = dataSnapshot1.getValue(Driver::class.java)!!
                        if (GL.isSignedIn()) {
                            if (model.uid!!.toLowerCase() != GL.getUserId().toLowerCase())
                                if (model.refOne!!.toLowerCase() == city!!.toLowerCase()
                                    || model.refTwo!!.toLowerCase() == city!!.toLowerCase()
                                    || model.refThree!!.toLowerCase() == city!!.toLowerCase()
                                    || model.refFour!!.toLowerCase() == city!!.toLowerCase()
                                    || model.refFive!!.toLowerCase() == city!!.toLowerCase()
                                )
                                    mList.add(0, model)
                        } else {
                            if (model.refOne!!.toLowerCase() == city!!.toLowerCase()
                                || model.refTwo!!.toLowerCase() == city!!.toLowerCase()
                                || model.refThree!!.toLowerCase() == city!!.toLowerCase()
                                || model.refFour!!.toLowerCase() == city!!.toLowerCase()
                                || model.refFive!!.toLowerCase() == city!!.toLowerCase()
                            )
                                mList.add(0, model)
                        }
                    }

                }
                mAdapter = RecyclerViewAdapter(mList, mListType, this@AllDrivers)
                drivers_list.addItemDecoration(
                    GridSpacingItemDecoration(
                        1,
                        dpToPx(
                            this@AllDrivers,
                            10
                        ),
                        true
                    )
                )
                drivers_list.itemAnimator = DefaultItemAnimator()
                drivers_list.adapter = mAdapter
            }
        })
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    inner class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) :
        RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private fun dpToPx(context: Context, dp: Int): Int {
        val r = context.resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView
        searchView.queryHint = getString(R.string.search_drivers)
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        when (mListType) {
            RecyclerViewAdapter.VIEW_TYPE_DRIVERS -> {

                val newList = ArrayList<Any>()
                for (model in mList) {
                    model as Driver
                    val name = model.name!!.toLowerCase()
                    if (name.contains(newText!!.toLowerCase()))
                        newList.add(model)
                }
                mAdapter!!.setFilter(newList)
            }
        }
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val action_map = menu!!.findItem(R.id.action_map)
        val action_chat = menu.findItem(R.id.action_chat)
        val action_search = menu.findItem(R.id.action_search)
        val action_add = menu.findItem(R.id.action_add)

        action_add.isVisible = false
        action_chat.isVisible = false
        action_map.isVisible = false

        return super.onPrepareOptionsMenu(menu)
    }
}
