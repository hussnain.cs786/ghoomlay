package org.ninesteps.ghoomlay.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_recover.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.sociotravel.helper.InputValidation

class RecoverActivity : AppCompatActivity(), View.OnClickListener {

    internal var inputValidation: InputValidation? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = "Recover Password"

        val reset = findViewById<Button>(R.id.btn_reset)

        reset.setOnClickListener(this)

        progressDialog = ProgressDialog(this)
        inputValidation = InputValidation(this)
    }

    override fun onClick(v: View?) {
        if (!inputValidation!!.isInputEditTextFilled(rp_email, rpe_layout, getString(R.string.error_message_email))) {
            return
        }
        if (!inputValidation!!.isInputEditTextEmail(rp_email, rpe_layout, getString(R.string.error_message_email))) {
            return
        } else {
            progressDialog!!.setTitle(getString(R.string.password_send))
            progressDialog!!.setMessage(getString(R.string.signing_up_message))
            progressDialog!!.setCanceledOnTouchOutside(false)
            progressDialog!!.show()
            GL.getAuth().sendPasswordResetEmail(rp_email.getText().toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        progressDialog!!.dismiss()
                        Toast.makeText(this, R.string.check_your_email, Toast.LENGTH_LONG).show()
                        onBackPressed()
                    } else {
                        progressDialog!!.hide()
                        Toast.makeText(this, R.string.error_message_email, Toast.LENGTH_LONG).show()
                    }
                }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
