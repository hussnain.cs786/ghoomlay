package org.ninesteps.ghoomlay.activity

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_driver_detail.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.helper.ExtendedDataHolder
import org.ninesteps.ghoomlay.model.Chat
import org.ninesteps.ghoomlay.model.Driver
import org.jetbrains.anko.toast


class DriverDetail : AppCompatActivity() {

    var type: String? = null
    var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_detail)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        type = intent.getStringExtra("TYPE")

        if (type == "profile") {
            username.text = GL.u!!.name
            userphone.text = "Phone Num: " + GL.u!!.phone
            usernic.text = "NIC: " + GL.u!!.nic
            useremail.text = "Email: " + GL.u!!.email

            vbrand.text = "Brand: " + GL.u!!.brand
            vcolor.text = "Color: " + GL.u!!.color
            vmodel.text = "Model: " + GL.u!!.model
            vyear.text = "Year: " + GL.u!!.year
            vicolor.text = "Interior color: " + GL.u!!.interiorColor

            lnum.text = "License Num: " + GL.u!!.licenseNumber
            vtype.text = "Vehicle Type: " + GL.u!!.vehicleValidType
            issueddate.text = "Issued date: " + GL.u!!.issuedDate
            expirydate.text = "Expiry Date: " + GL.u!!.expiryDate

            r1.text = "Ref. City 1: ${GL!!.u!!.refOne}"
            r2.text = "Ref. City 2: ${GL!!.u!!.refTwo}"
            r3.text = "Ref. City 3: ${GL!!.u!!.refThree}"
            r4.text = "Ref. City 4: ${GL!!.u!!.refFour}"
            r5.text = "Ref. City 5: ${GL!!.u!!.refFive}"

            o1.text = "OFFER 1: ${GL!!.u!!.offerOne}"
            o2.text = "OFFER 1: ${GL!!.u!!.offerTwo}"
            o3.text = "OFFER 1: ${GL!!.u!!.offerThree}"

            val bitmap = stringToBitMap(GL.u!!.thumbnail!!)
            val bitmap1 = stringToBitMap(GL.u!!.licenseThumbnail!!)
            val bitmap2 = stringToBitMap(GL.u!!.vehicleThumbnail!!)

            userimage.setImageBitmap(bitmap)
            userlthumbnail.setImageBitmap(bitmap1)
            uservthumbnail.setImageBitmap(bitmap2)

            button.visibility = View.GONE

            if (GL.u!!.userType.equals("driver")) {
                lvdetail.visibility = View.VISIBLE
            } else {
                lvdetail.visibility = View.GONE
            }

        } else if (type == "request") {

            mProgressDialog = ProgressDialog(this)
            Log.e("TYPE", type)
            val extras = ExtendedDataHolder.getInstance()
            if (extras.hasExtra("DR-MODEL")) {
                val model = extras.getExtra("DR-MODEL") as Driver
                Log.e("MODEL", model.name)

                username.text = model.name
                userphone.text = "Phone Num: " + model.phone
                usernic.text = "NIC: " + model.nic
                useremail.text = "Email: " + model.email

                vbrand.text = "Brand: " + model.brand
                vcolor.text = "Color: " + model.color
                vmodel.text = "Model: " + model.model
                vyear.text = "Year: " + model.year
                vicolor.text = "Interior color: " + model.interiorColor

                lnum.text = "License Num: " + model.licenseNumber
                vtype.text = "Vehicle Type: " + model.vehicleValidType
                issueddate.text = "Issued date: " + model.issuedDate
                expirydate.text = "Expiry Date: " + model.expiryDate

                r1.text = "Ref. City 1: ${model.refOne}"
                r2.text = "Ref. City 2: ${model.refTwo}"
                r3.text = "Ref. City 3: ${model.refThree}"
                r4.text = "Ref. City 4: ${model.refFour}"
                r5.text = "Ref. City 5: ${model.refFive}"

                o1.text = "OFFER 1: ${model.offerOne}"
                o2.text = "OFFER 1: ${model.offerTwo}"
                o3.text = "OFFER 1: ${model.offerThree}"

                val bitmap = stringToBitMap(model.thumbnail!!)
                val bitmap1 = stringToBitMap(model.licenseThumbnail!!)
                val bitmap2 = stringToBitMap(model.vehicleThumbnail!!)

                userimage.setImageBitmap(bitmap)
                userlthumbnail.setImageBitmap(bitmap1)
                uservthumbnail.setImageBitmap(bitmap2)

                button.visibility = View.VISIBLE
                button.text = "Accept Request"
                button.setOnClickListener {
                    mProgressDialog!!.setTitle("Processing")
                    mProgressDialog!!.setMessage("Please wait...")
                    mProgressDialog!!.setCanceledOnTouchOutside(false)
                    mProgressDialog!!.show()

                    val k = GL.getDatabaseRef().child("all-drivers").push().key

                    val d = Driver(
                        k,
                        model.uid, model.name, model.email, model.password, model.phone, model.nic, model.thumbnail,
                        model.userType, model.brand, model.model, model.year, model.color, model.interiorColor,
                        model.vehicleThumbnail, model.licenseNumber, model.vehicleValidType, model.issuedDate,
                        model.expiryDate, model.licenseThumbnail,
                        model.refOne, model.refTwo, model.refThree, model.refFour, model.refFive,
                        model.offerOne, model.offerTwo, model.offerThree
                    )

                    val refer = GL.getDatabaseRef().child("all-drivers").child(k)
                    refer.setValue(d).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mProgressDialog!!.dismiss()
                            Toast.makeText(this, "Request Accepted", Toast.LENGTH_LONG).show()
                            GL.getDatabaseRef().child("pending-drivers").child(model.key).removeValue()
                        } else {
                            mProgressDialog!!.dismiss()
                            toast("Something went wrong!")
                        }
                    }

                }
            }
        } else if (type == "accept") {

            mProgressDialog = ProgressDialog(this)
            Log.e("TYPE", type)
            val extras = ExtendedDataHolder.getInstance()
            if (extras.hasExtra("DRIVER-MODEL")) {
                val model = extras.getExtra("DRIVER-MODEL") as Driver
                Log.e("MODEL", model.name)

                username.text = model.name
                userphone.text = "Phone Num: " + model.phone
                usernic.text = "NIC: " + model.nic
                useremail.text = "Email: " + model.email

                vbrand.text = "Brand: " + model.brand
                vcolor.text = "Color: " + model.color
                vmodel.text = "Model: " + model.model
                vyear.text = "Year: " + model.year
                vicolor.text = "Interior color: " + model.interiorColor

                lnum.text = "License Num: " + model.licenseNumber
                vtype.text = "Vehicle Type: " + model.vehicleValidType
                issueddate.text = "Issued date: " + model.issuedDate
                expirydate.text = "Expiry Date: " + model.expiryDate

                r1.text = "Ref. City 1: ${model.refOne}"
                r2.text = "Ref. City 2: ${model.refTwo}"
                r3.text = "Ref. City 3: ${model.refThree}"
                r4.text = "Ref. City 4: ${model.refFour}"
                r5.text = "Ref. City 5: ${model.refFive}"

                o1.text = "OFFER 1: ${model.offerOne}"
                o2.text = "OFFER 1: ${model.offerTwo}"
                o3.text = "OFFER 1: ${model.offerThree}"

                val bitmap = stringToBitMap(model.thumbnail!!)
                val bitmap1 = stringToBitMap(model.licenseThumbnail!!)
                val bitmap2 = stringToBitMap(model.vehicleThumbnail!!)

                userimage.setImageBitmap(bitmap)
                userlthumbnail.setImageBitmap(bitmap1)
                uservthumbnail.setImageBitmap(bitmap2)

                button.visibility = View.VISIBLE
                button.text = "Send Message"
                button.setOnClickListener {

                    if (GL.isSignedIn()) {
                        mProgressDialog!!.setTitle("Processing...")
                        mProgressDialog!!.setMessage("Please wait we are creating your chat.")
                        mProgressDialog!!.setCanceledOnTouchOutside(false)
                        mProgressDialog!!.show()
                        val roomid = model.uid+GL.getUserId()
                        val senderChat = Chat(model.name, model.thumbnail, model.uid,GL.u!!.name,GL.u!!.thumbnail,GL.getUserId(),roomid)
                        val receiverChat = Chat(GL.u!!.name,GL.u!!.thumbnail,GL.getUserId(),model.name, model.thumbnail, model.uid,roomid)
                        GL.getDatabaseRef().child("all-chats").child(GL.getUserId()).child(model.uid).setValue(senderChat)
                        GL.getDatabaseRef().child("all-chats").child(model.uid).child(GL.getUserId()).setValue(receiverChat)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    mProgressDialog!!.dismiss()
                                    val extra = ExtendedDataHolder.getInstance()
                                    extra.putExtra("extra", ByteArray(1024 * 1024))
                                    val i = Intent(this, ChatActivity::class.java)
                                    i.putExtra("CHAT-NAME", model.name)
                                    extra.putExtra("CHAT-IMAGE", model.thumbnail)
                                    i.putExtra("CHAT-ID", model.uid)
                                    i.putExtra("ROOM-ID",roomid)
                                    startActivity(i)
                                } else {
                                    mProgressDialog!!.dismiss()
                                    Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_LONG)
                                        .show()
                                }
                            }
                    } else {
                        startActivity(Intent(this@DriverDetail, LoginActivity::class.java))
                        // close this activity
//                        finish()
                    }

                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }
}
