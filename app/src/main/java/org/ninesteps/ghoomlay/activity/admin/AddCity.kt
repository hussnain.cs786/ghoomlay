package org.ninesteps.ghoomlay.activity.admin

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_add_city.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.City
import org.ninesteps.sociotravel.helper.InputValidation
import java.io.ByteArrayOutputStream
import java.io.IOException
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.helpers.Constants
import com.darsh.multipleimageselect.models.Image
import kotlinx.android.synthetic.main.activity_add_point.*
import org.jetbrains.anko.toast
import java.io.File


//import com.google.android.libraries.places.compat.Place;

class AddCity : AppCompatActivity() {

    var address = "Lahore"
    var latitude = 31.4826352
    var longitude = 74.054189

//    var address = "default"
//    var longitude = "default"
//    var latitude = "default"

    internal var encodedImage = "thumbnail"
    internal var encodedImage1 = "thumbnail"
    internal var encodedImage2 = "thumbnail"
    internal var encodedImage3 = "thumbnail"
    internal var encodedImage4 = "thumbnail"
    internal var encodedImage5 = "thumbnail"

    internal var name: String? = null
    internal var desc: String? = null
    internal var province: String? = null

    private val GALLERY = 100
    private val CAMERA = 10

    internal var inputValidation_: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_city)

//        if (!Places.isInitialized()) {
//            Places.initialize(applicationContext, resources.getString(R.string.google_api_key));
//        }

        title = resources.getString(R.string.add_city)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        inputValidation_ = InputValidation(this)
        mProgress = ProgressDialog(this)

        val autocompleteFragment =
            fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment

        /*AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(filter);*/
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onError(status: Status?) {
                Toast.makeText(this@AddCity, status.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onPlaceSelected(place: Place) {
                city_address.text = place.name.toString()
                Log.e("NAME:", place.name.toString())
                Log.e("LONGITUDE", place.latLng.longitude.toString())
                Log.e("LATITUDE", place.latLng.latitude.toString())

                address = place.name.toString()
                longitude = place.latLng.longitude
                latitude = place.latLng.latitude
            }


        })

        browse_cimage.setOnClickListener {
//            chooseMultipleImage()
            imageDialog()
        }

        add_city.setOnClickListener {
            name = city_name.text.toString()
            desc = city_desc.text.toString()
            province = city_province.text.toString()

            if (encodedImage.equals("thumbnail")) {
                Toast.makeText(this, "Please select image", Toast.LENGTH_LONG).show()
            } else if (address.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else if (longitude.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else if (latitude.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else
                if (!inputValidation_!!.isInputEditTextFilled(
                        city_name,
                        cname_layout,
                        "Enter city name"
                    )
                ) {
                    return@setOnClickListener
                } else
                    if (!inputValidation_!!.isInputEditTextFilled(
                            city_desc,
                            cdesc_layout,
                            "Enter city description"
                        )
                    ) {
                        return@setOnClickListener
                    } else
                        if (!inputValidation_!!.isInputEditTextFilled(
                                city_province,
                                cprovince_layout,
                                "Enter Province description"
                            )
                        ) {
                            return@setOnClickListener
                        } else {
                            //...
                            mProgress!!.setTitle("Adding...")
                            mProgress!!.setMessage("Please wait while we check your credentials.")
                            mProgress!!.setCanceledOnTouchOutside(false)
                            mProgress!!.show()

                            val city = City(
                                name,
                                desc,
                                province,
                                address,
                                longitude,
                                latitude,
                                encodedImage
                            )
                            val key = GL.getDatabaseRef().child("cities").push().getKey()
                            GL.getDatabaseRef().child("cities").child(key)
                                .setValue(city)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        //...
                                        mProgress!!.dismiss()
                                        finish()
                                        Toast.makeText(this,"Data added successfully!",Toast.LENGTH_LONG).show()
                                    } else {
                                        //...
                                        mProgress!!.dismiss()
                                        Toast.makeText(this,"Something went wrong",Toast.LENGTH_LONG).show()
                                    }
                                }
                        }

        }
    }

    fun chooseMultipleImage(){
        val intent = Intent(this, AlbumSelectActivity::class.java)
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5)
        startActivityForResult(intent, Constants.REQUEST_CODE)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        city_thumbnail.setImageBitmap(bit)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            val images:ArrayList<Image> = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES)

            Log.e("IMAGE: ", images.size.toString())

            if (images.size < 5){
                toast("Please select 5 images.")
            }else {
                val uri1: Uri = Uri.fromFile(File(images[0].path))
                val uri2: Uri = Uri.fromFile(File(images[1].path))
                val uri3: Uri = Uri.fromFile(File(images[2].path))
                val uri4: Uri = Uri.fromFile(File(images[3].path))
                val uri5: Uri = Uri.fromFile(File(images[4].path))

                var bit1: Bitmap? = null
                var bit2: Bitmap? = null
                var bit3: Bitmap? = null
                var bit4: Bitmap? = null
                var bit5: Bitmap? = null
                try {
                    bit1 = MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
                    bit2 = MediaStore.Images.Media.getBitmap(this.contentResolver, uri2)
                    bit3 = MediaStore.Images.Media.getBitmap(this.contentResolver, uri3)
                    bit4 = MediaStore.Images.Media.getBitmap(this.contentResolver, uri4)
                    bit5 = MediaStore.Images.Media.getBitmap(this.contentResolver, uri5)
                    city_thumbnail.setImageBitmap(bit1)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                val byteArrayOutputStream1 = ByteArrayOutputStream()
                val byteArrayOutputStream2 = ByteArrayOutputStream()
                val byteArrayOutputStream3 = ByteArrayOutputStream()
                val byteArrayOutputStream4 = ByteArrayOutputStream()
                val byteArrayOutputStream5 = ByteArrayOutputStream()

                bit1!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream1)
                bit2!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream2)
                bit3!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream3)
                bit4!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream4)
                bit5!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream5)
                encodedImage1 = Base64.encodeToString(byteArrayOutputStream1.toByteArray(), Base64.DEFAULT)
                encodedImage2 = Base64.encodeToString(byteArrayOutputStream2.toByteArray(), Base64.DEFAULT)
                encodedImage3 = Base64.encodeToString(byteArrayOutputStream3.toByteArray(), Base64.DEFAULT)
                encodedImage4 = Base64.encodeToString(byteArrayOutputStream4.toByteArray(), Base64.DEFAULT)
                encodedImage5 = Base64.encodeToString(byteArrayOutputStream5.toByteArray(), Base64.DEFAULT)

            }
        }
    }
    */
}
