package org.ninesteps.ghoomlay.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.chat_list.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.Chat

class ChatList : AppCompatActivity() {

    private var mListType = RecyclerViewAdapter.VIEW_TYPE_CHAT
    private var mAdapter: RecyclerViewAdapter? = null
    var mList: MutableList<Any> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_list)

        title = "Chat"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        GL.getDatabaseRef().child("all-chats").child(GL.getUserId()).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("CHATS", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {

                    val model = dataSnapshot1.getValue(Chat::class.java)!!
                        mList.add(0, model)
                }
                mAdapter = RecyclerViewAdapter(mList, mListType, this@ChatList)
                chat_list.adapter = mAdapter
            }
        })


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
