package org.ninesteps.ghoomlay.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_city_detail.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.activity.admin.AddPoint
import org.ninesteps.ghoomlay.helper.ExtendedDataHolder
import org.ninesteps.ghoomlay.model.City
import org.ninesteps.ghoomlay.model.Map
import java.util.HashMap
import kotlin.collections.ArrayList


class CityDetail : AppCompatActivity(){

    var model: City? = null
    var mapList: MutableList<Map> = ArrayList()

    var loc: ArrayList<String> = ArrayList()
    var lon: ArrayList<String> = ArrayList()
    var lat: ArrayList<String> = ArrayList()


    var i = 0
    internal lateinit var file_maps: HashMap<String, Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_detail)

        val extras = ExtendedDataHolder.getInstance()
        if (extras.hasExtra(GL.MODEL_CITY)) {

            model = extras.getExtra(GL.MODEL_CITY) as City

            title = model!!.name
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

            cd_name.text = model!!.name
            val bitmap = stringToBitMap(model!!.thumbnail1.toString())

            getMapData()

            cd_image.setImageBitmap(bitmap)

            sightseeing.setOnClickListener {
                val intent = Intent(this, PointsActivity::class.java)
                intent.putExtra(GL.POINT_TYPE, "Sightseeing")
                intent.putExtra(GL.POINT_CITY, model!!.name)
                startActivity(intent)
            }

            nightlife.setOnClickListener {
                val intent = Intent(this, PointsActivity::class.java)
                intent.putExtra(GL.POINT_TYPE, "Nightlife")
                intent.putExtra(GL.POINT_CITY, model!!.name)
                startActivity(intent)
            }

            hotels.setOnClickListener {
                val intent = Intent(this, PointsActivity::class.java)
                intent.putExtra(GL.POINT_TYPE, "Hotels")
                intent.putExtra(GL.POINT_CITY, model!!.name)
                startActivity(intent)
            }

            eatanddrinks.setOnClickListener {
                val intent = Intent(this, PointsActivity::class.java)
                intent.putExtra(GL.POINT_TYPE, "Eats and Drinks")
                intent.putExtra(GL.POINT_CITY, model!!.name)
                startActivity(intent)
            }

            recommended_drivers.setOnClickListener {
                val intent = Intent(this@CityDetail, AllDrivers::class.java)
                intent.putExtra(GL.DRIVER_STATUS, "recommended")
                intent.putExtra(GL.DRIVER_CITY, model!!.name)
                startActivity(intent)
            }

        }
    }

    fun getMapData() {
        GL.getDatabaseRef().child("points").child(model!!.name).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                i = 0
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("POINTS", "OK")
                mapList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {
                    loc.add(dataSnapshot1.child("location").value.toString())
                    lon.add(dataSnapshot1.child("longitude").value.toString())
                    lat.add(dataSnapshot1.child("latitude").value.toString())
                }
                i = 1
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_add -> {
                val intent = Intent(this, AddPoint::class.java)
                intent.putExtra("CITY", model!!.name)
                startActivity(intent)
                true
            }
            R.id.action_map -> {
                val intent = Intent(this, MapsActivity::class.java)
                intent.putExtra(GL.MAP_STATUS, "list")
                intent.putExtra("LOC", loc)
                intent.putExtra("LON", lon)
                intent.putExtra("LAT", lat)
                startActivity(intent)
                true
            }
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val action_map = menu!!.findItem(R.id.action_map)
        val action_chat = menu.findItem(R.id.action_chat)
        val action_search = menu.findItem(R.id.action_search)
        val action_add = menu.findItem(R.id.action_add)

        action_chat.isVisible = false
        action_search.isVisible = false

        if (GL.isSignedIn())
            action_add.isVisible = GL.u!!.userType.equals("admin")
        else
            action_add.isVisible = false

        return super.onPrepareOptionsMenu(menu)
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }
}
