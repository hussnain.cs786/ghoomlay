package org.ninesteps.ghoomlay.activity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import kotlinx.android.synthetic.main.activity_chat_list.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.adapter.MessageAdapter
import org.ninesteps.ghoomlay.helper.ExtendedDataHolder
import org.ninesteps.ghoomlay.model.Conversation
import org.ninesteps.ghoomlay.model.Message

class ChatActivity : AppCompatActivity() {

    var roomId: String? = null
    var friendName: String? = null
    var TAG = "ChatActivity"

    var name: String? = null     //friend name
    var image: String? = null    //friend image
    var uid: String? = null      //friend id

    var adapter: MessageAdapter? = null
    private var conversation: Conversation? = null
    private var linearLayoutManager: LinearLayoutManager? = null

    var bitmapUser: Bitmap? = null
    var bitmapFreind: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        conversation = Conversation()

        val extras = ExtendedDataHolder.getInstance()
        if (extras.hasExtra("CHAT-IMAGE")) {
            image = extras.getExtra("CHAT-IMAGE") as String
        }

        name = intent.getStringExtra("CHAT-NAME")
        uid = intent.getStringExtra("CHAT-ID")

        title = name
        roomId = intent.getStringExtra("ROOM-ID")
        friendName = name

        bitmapFreind = stringToBitMap(image!!)
        bitmapUser = stringToBitMap(GL.u!!.thumbnail.toString())

        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerChat.layoutManager = linearLayoutManager

        adapter = MessageAdapter(this, conversation!!, bitmapFreind!!, bitmapUser!!)

        val childEventListener = object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.key!!)

                val message = dataSnapshot.getValue(Message::class.java)

                conversation!!.listMessageData.add(message)
                adapter!!.notifyDataSetChanged()
                linearLayoutManager!!.scrollToPosition(conversation!!.listMessageData.size - 1)

            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                Log.d(TAG, "onChildChanged: ${dataSnapshot.key}")

            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                Log.d(TAG, "onChildRemoved:" + dataSnapshot.key!!)

            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
                Log.d(TAG, "onChildMoved:" + dataSnapshot.key!!)

            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w(TAG, "postComments:onCancelled", databaseError.toException())
                Toast.makeText(
                    this@ChatActivity, "Failed to load comments.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        GL.getDatabaseRef().child("message").child(roomId).addChildEventListener(childEventListener)
        recyclerChat.adapter = adapter


        btn_send_message.setOnClickListener {
            val content = edit_write_message.text.toString().trim()
            if (content.isNotEmpty()) {
                val msg = Message(GL.getUserId(), content, System.currentTimeMillis())
                GL.getDatabaseRef().child("message").child(roomId).push().setValue(msg)
                edit_write_message.setText("")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }
}
