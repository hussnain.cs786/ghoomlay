package org.ninesteps.ghoomlay.activity

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_login.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.User
import org.ninesteps.sociotravel.helper.InputValidation

class LoginActivity : AppCompatActivity(), View.OnClickListener {


    internal var e: String = ""
    internal var p: String = ""
    internal var alert: AlertDialog.Builder? = null

    internal var inputValidation: InputValidation? = null
    internal var mLoginProgress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        inputValidation = InputValidation(this)
        mLoginProgress = ProgressDialog(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        title = "Login"

        alert = AlertDialog.Builder(this)
        alert!!.setTitle("Are you?")
        alert!!.setMessage("Driver or Customer")

        val login_btn: Button = findViewById(R.id.login)
        val create_new_account: Button = findViewById(R.id.new_account)
        val forgot_password: Button = findViewById(R.id.forgot_password)

        login_btn.setOnClickListener(this)
        create_new_account.setOnClickListener(this)
        forgot_password.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.login -> {

                e = login_email.getText().toString()
                p = login_pass.getText().toString()

                if (!inputValidation!!.isInputEditTextFilled(
                        login_email,
                        e_layout,
                        getString(R.string.error_message_email)
                    )
                ) {
                    return
                }
                if (!inputValidation!!.isInputEditTextEmail(
                        login_email,
                        e_layout,
                        getString(R.string.error_message_email)
                    )
                ) {
                    return
                }
                if (!inputValidation!!.isInputEditTextFilled(
                        login_pass,
                        p_layout,
                        getString(R.string.error_message_password)
                    )
                ) run {
                    return
                }
                else {
                    mLoginProgress!!.setTitle("Logging In")
                    mLoginProgress!!.setMessage("Please wait while we check your credentials.")
                    mLoginProgress!!.setCanceledOnTouchOutside(false)
                    mLoginProgress!!.show()

                    GL.getAuth().signInWithEmailAndPassword(e, p)
                        .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                            override fun onComplete(task: Task<AuthResult>) {
                                if (task.isSuccessful()) {
                                    GL.getDatabaseRef()
                                        .child("users")
                                        .child(GL.getUserId())
                                        .addListenerForSingleValueEvent(object : ValueEventListener {
                                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                                Log.e("OK", "OK")
                                                val u = dataSnapshot.getValue(User::class.java)
                                                if (u != null) {
                                                    GL.u = u
                                                    mLoginProgress!!.dismiss()
                                                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                    startActivity(intent)
                                                } else {
                                                    Toast.makeText(
                                                        this@LoginActivity,
                                                        R.string.something_went_wrong,
                                                        Toast.LENGTH_LONG
                                                    ).show()
                                                }
                                            }

                                            override fun onCancelled(databaseError: DatabaseError) {
                                                mLoginProgress!!.dismiss()
                                                Log.e("Error", databaseError.toException().message)
                                            }
                                        })

                                    Log.d("Login", "signInWithEmail:success")

                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w("Login", "signInWithEmail:failure", task.getException())
                                    mLoginProgress!!.dismiss()
                                    Toast.makeText(
                                        this@LoginActivity,
                                        "Error : " + task.getException()!!.message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        })
                }

            }
            R.id.new_account -> {
                alert!!.setPositiveButton("I'm Driver", DialogInterface.OnClickListener { _, i ->
                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                    intent.putExtra(GL.USERTTYPE, "driver")
                    startActivity(intent)
                })
                alert!!.setNegativeButton("I'm User") { _, i ->
                    val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
                    intent.putExtra(GL.USERTTYPE, "user")
                    startActivity(intent)
                }
                alert!!.show()
            }
            R.id.forgot_password -> {
                startActivity(Intent(this@LoginActivity, RecoverActivity::class.java))
            }
        }
    }
}
