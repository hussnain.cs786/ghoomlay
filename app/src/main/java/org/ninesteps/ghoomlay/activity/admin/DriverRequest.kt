package org.ninesteps.ghoomlay.activity.admin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.driver_request_list.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.Driver

class DriverRequest : AppCompatActivity() {

    private var mListType = RecyclerViewAdapter.VIEW_TYPE_REQUESTS
    private var mAdapter: RecyclerViewAdapter? = null
    var mList: MutableList<Any> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.driver_request_list)

        title = "Driver's Requests"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        GL.getDatabaseRef().child("pending-drivers").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("REQUESTS", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {

                    val model = dataSnapshot1.getValue(Driver::class.java)!!
                        mList.add(0, model)

                }
                mAdapter = RecyclerViewAdapter(mList, mListType, this@DriverRequest)
                dr_list.adapter = mAdapter
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
