package org.ninesteps.ghoomlay.activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_points_list.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.Point

class PointsActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    private var mListType = RecyclerViewAdapter.VIEW_TYPE_POINTS
    private var mAdapter: RecyclerViewAdapter? = null
    var mList: MutableList<Any> = ArrayList()

    var loc:ArrayList<String> = ArrayList()
    var lon:ArrayList<String> = ArrayList()
    var lat:ArrayList<String> = ArrayList()

    var city: String? = null
    var type: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_points_list)

        city = intent.getStringExtra(GL.POINT_CITY)
        type = intent.getStringExtra(GL.POINT_TYPE)

        title = type
        supportActionBar!!.subtitle = city
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        GL.getDatabaseRef().child("points").child(city).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("POINTS", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {

                    Log.e("POINTS-DATA",dataSnapshot1.child("type").value.toString())
                    Log.e("DATA",type)
                    val model = dataSnapshot1.getValue(Point::class.java)!!
                    if (model.type!!.toLowerCase() == type.toString().toLowerCase()) {
                        mList.add(0, model)
                        loc.add(model.location!!)
                        lon.add(model.longitude!!.toString())
                        lat.add(model.latitude!!.toString())
                    }
                }
                mAdapter = RecyclerViewAdapter(mList, mListType, this@PointsActivity)
                points_list.adapter = mAdapter
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView
        searchView.queryHint = "Search ${type!!.toLowerCase()}"
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_map->{
                val intent = Intent(this, MapsActivity::class.java)
                intent.putExtra(GL.MAP_STATUS, "list")
                intent.putExtra("LOC",loc)
                intent.putExtra("LON",lon)
                intent.putExtra("LAT",lat)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        when (mListType) {
            RecyclerViewAdapter.VIEW_TYPE_POINTS -> {

                val newList = ArrayList<Any>()
                for (model in mList) {
                    model as Point
                    val name = model.name!!.toLowerCase()
                    val tag1 = model.tagone!!.toLowerCase()
                    val tag2 = model.tagtwo!!.toLowerCase()
                    val tag3 = model.tagthree!!.toLowerCase()
                    if (name.contains(newText!!.toLowerCase())
                        or tag1.contains(newText.toLowerCase())
                    or tag2.contains(newText.toLowerCase())
                    or tag3.contains(newText.toLowerCase()))
                        newList.add(model)
                }
                mAdapter!!.setFilter(newList)
            }
        }
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val action_map = menu!!.findItem(R.id.action_map)
        val action_chat = menu.findItem(R.id.action_chat)
        val action_search = menu.findItem(R.id.action_search)
        val action_add = menu.findItem(R.id.action_add)

        action_add.isVisible = false
        action_chat.isVisible = false

        return super.onPrepareOptionsMenu(menu)
    }
}
