package org.ninesteps.ghoomlay.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_license.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.activity.driver.RefCityActivity
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class LicenseActivity : AppCompatActivity(), View.OnClickListener {

    private val GALLERY = 100
    private val CAMERA = 101
    internal var encodedImage = "thumbnail"

    var ln: String? = null
    var vt: String? = null
    var id: String? = null
    var ed: String? = null

    var cal: Calendar? = null
    var c: Calendar? = null

    var mProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_license)

        mProgressDialog = ProgressDialog(this)
        title = "License Details"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val btnNext = findViewById<Button>(R.id.license_btn)
        val btnIssued = findViewById<Button>(R.id.btn_issued_date)
        val btnExpiry = findViewById<Button>(R.id.btn_expiry_date)

        cal = Calendar.getInstance()
        c = Calendar.getInstance()

        btnExpiry.setOnClickListener(this)
        btnIssued.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        licenseImage.setOnClickListener {
            imageDialog()
        }
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.btn_issued_date -> {

                DatePickerDialog(
                    this, idate, c!!
                        .get(Calendar.YEAR), c!!.get(Calendar.MONTH),
                    c!!.get(Calendar.DAY_OF_MONTH)
                ).show()

            }
            R.id.btn_expiry_date -> {

                DatePickerDialog(
                    this, edate, cal!!
                        .get(Calendar.YEAR), cal!!.get(Calendar.MONTH),
                    cal!!.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
            R.id.license_btn -> {
                ln = license_no.text.toString()
                vt = vehicle_type.text.toString()

                if (encodedImage.equals("thumbnail")){
                    Toast.makeText(this,"Please select image",Toast.LENGTH_LONG).show()
                }else
                if (!ln!!.isEmpty()
                    || !vt!!.isEmpty()
                    || !id!!.isEmpty()
                    || !ed!!.isEmpty()
                ) {

                    val ref1 = GL.getDatabaseRef().child("users").child(GL.getUserId())

                    val map = HashMap<String, String>()

                    map.put("uid", GL.u!!.uid!!)
                    map.put("name", GL.u!!.name!!)
                    map.put("email", GL.u!!.email!!)
                    map.put("password", GL.u!!.password!!)
                    map.put("phone", GL.u!!.phone!!)
                    map.put("nic", GL.u!!.nic!!)
                    map.put("thumbnail", GL.u!!.thumbnail!!)
                    map.put("userType", "driver")

                    map.put("brand", GL.u!!.brand!!)
                    map.put("model", GL.u!!.model!!)
                    map.put("year", GL.u!!.year!!)
                    map.put("color", GL.u!!.color!!)
                    map.put("interiorColor", GL.u!!.interiorColor!!)
                    map.put("vehicleThumbnail", GL.u!!.vehicleThumbnail!!)

                    map.put("licenseNumber", ln!!)
                    map.put("vehicleValidType", vt!!)
                    map.put("issuedDate", id!!)
                    map.put("expiryDate", ed!!)
                    map.put("licenseThumbnail", encodedImage)

                    map.put("refOne", GL.u!!.refOne!!)
                    map.put("refTwo", GL.u!!.refTwo!!)
                    map.put("refThree", GL.u!!.refThree!!)
                    map.put("refFour", GL.u!!.refFour!!)
                    map.put("refFive", GL.u!!.refFive!!)

                    map.put("offerOne", GL.u!!.offerOne!!)
                    map.put("offerTwo", GL.u!!.offerTwo!!)
                    map.put("offerThree", GL.u!!.offerThree!!)

                    mProgressDialog!!.setTitle("Processing")
                    mProgressDialog!!.setMessage("Please wait...")
                    mProgressDialog!!.setCanceledOnTouchOutside(false)
                    mProgressDialog!!.show()

                    ref1.setValue(map).addOnCompleteListener { task ->
                        if (task.isSuccessful) {

                            mProgressDialog!!.dismiss()

                            GL.u!!.uid = GL.u!!.uid!!
                            GL.u!!.name = GL.u!!.name!!
                            GL.u!!.email = GL.u!!.email!!
                            GL.u!!.password = GL.u!!.password!!
                            GL.u!!.phone = GL.u!!.phone!!
                            GL.u!!.nic = GL.u!!.nic!!
                            GL.u!!.thumbnail = GL.u!!.thumbnail!!
                            GL.u!!.userType = "driver"
                            GL.u!!.brand = GL.u!!.brand!!
                            GL.u!!.model = GL.u!!.model!!
                            GL.u!!.year = GL.u!!.year!!
                            GL.u!!.color = GL.u!!.color!!
                            GL.u!!.interiorColor = GL.u!!.interiorColor!!
                            GL.u!!.vehicleThumbnail = GL.u!!.vehicleThumbnail!!
                            GL.u!!.licenseNumber = ln!!
                            GL.u!!.vehicleValidType = vt!!
                            GL.u!!.issuedDate = id!!
                            GL.u!!.expiryDate = ed!!
                            GL.u!!.licenseThumbnail = encodedImage

                            GL.u!!.refOne = GL.u!!.refOne
                            GL.u!!.refTwo = GL.u!!.refTwo
                            GL.u!!.refThree = GL.u!!.refThree
                            GL.u!!.refFour = GL.u!!.refFour
                            GL.u!!.refFive = GL.u!!.refFive

                            GL.u!!.offerOne = GL.u!!.offerOne
                            GL.u!!.offerTwo = GL.u!!.offerTwo
                            GL.u!!.offerThree = GL.u!!.offerThree

                            startActivity(Intent(this, RefCityActivity::class.java))
                        }
                    }

                } else {
                    Toast.makeText(this, "All fields are required!", Toast.LENGTH_LONG).show()
                }

            }
        }

    }

    internal var idate: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            c!!.set(Calendar.YEAR, year)
            c!!.set(Calendar.MONTH, monthOfYear)
            c!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MMM dd, yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            issue_date.text = sdf.format(c!!.getTime())
            id = sdf.format(c!!.getTime())
        }

    internal var edate: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            cal!!.set(Calendar.YEAR, year)
            cal!!.set(Calendar.MONTH, monthOfYear)
            cal!!.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MMM dd, yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            expiry_date.text = sdf.format(cal!!.getTime())
            ed = sdf.format(cal!!.getTime())
        }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        license_thumbnail.setImageBitmap(bit)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
