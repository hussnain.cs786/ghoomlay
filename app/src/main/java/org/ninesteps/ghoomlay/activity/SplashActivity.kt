package org.ninesteps.ghoomlay.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.User
import java.util.ArrayList

class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val rationale = "Please provide location permission so that you can ..."
        val options = Permissions.Options()
            .setRationaleDialogTitle("Info")
            .setSettingsDialogTitle("Warning")

        Permissions.check(this/*context*/, permissions, rationale, options, object : PermissionHandler() {
            override fun onGranted() {
                window.setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
                )

                Handler().postDelayed({
                    if (GL.isSignedIn()) {
                        Log.e("isSignIn", "User is signed in " + GL.getUserId())
                        GL.getDatabaseRef()
                            .child("users")
                            .child(GL.getUserId())
                            .addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    Log.e("OK", "OK")
                                    val u = dataSnapshot.getValue<User>(User::class.java)
                                    if (u != null) {
                                        GL.u = u
                                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                                        // close this activity
                                        finish()
                                    } else {
                                        Toast.makeText(
                                            applicationContext,
                                            "Something went wrong please try again.",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }

                                override fun onCancelled(databaseError: DatabaseError) {
                                    Log.e("Error", databaseError.toException().message)
                                }
                            })
                    } else {
                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                        // close this activity
                        finish()
                    }
                }, SPLASH_TIME_OUT.toLong())
            }

            override fun onDenied(context: Context, deniedPermissions: ArrayList<String>) {
                finish()
            }
        })


    }
}
