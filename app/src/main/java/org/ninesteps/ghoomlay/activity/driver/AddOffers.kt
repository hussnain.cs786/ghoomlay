package org.ninesteps.ghoomlay.activity.driver

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_add_offers.*
import kotlinx.android.synthetic.main.activity_register.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.activity.MainActivity
import org.ninesteps.ghoomlay.model.Driver
import org.ninesteps.sociotravel.helper.InputValidation
import java.util.HashMap

class AddOffers : AppCompatActivity() {

    var offer1: String? = null
    var offer2: String? = null
    var offer3: String? = null

    var mProgressDialog: ProgressDialog? = null
    internal var inputValidation_: InputValidation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_offers)

        mProgressDialog = ProgressDialog(this)
        inputValidation_ = InputValidation(this)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = resources.getString(R.string.add_offers)

        add_offers.setOnClickListener {
            offer1 = offer01.text.toString()
            offer2 = offer02.text.toString()
            offer3 = offer03.getText().toString()

            if (!inputValidation_!!.isInputEditTextFilled(
                    offer01,
                    offer1_layout,
                    getString(R.string.error_message_offer)
                )
            ) {
                return@setOnClickListener
            }
            if (!inputValidation_!!.isInputEditTextFilled(
                    offer02,
                    offer2_layout,
                    getString(R.string.error_message_offer)
                )
            ) {
                return@setOnClickListener
            }
            if (!inputValidation_!!.isInputEditTextFilled(
                    offer03,
                    offer3_layout,
                    getString(R.string.error_message_offer)
                )
            ) {
                return@setOnClickListener
            }else{
                val ref1 = GL.getDatabaseRef().child("users").child(GL.getUserId())

                val k = GL.getDatabaseRef().child("pending-drivers").push().key
                val refer = GL.getDatabaseRef().child("pending-drivers").child(k)

                val map = HashMap<String, String>()

                map.put("uid", GL.u!!.uid!!)
                map.put("name", GL.u!!.name!!)
                map.put("email", GL.u!!.email!!)
                map.put("password", GL.u!!.password!!)
                map.put("phone", GL.u!!.phone!!)
                map.put("nic", GL.u!!.nic!!)
                map.put("thumbnail", GL.u!!.thumbnail!!)
                map.put("userType", "driver")

                map.put("brand", GL.u!!.brand!!)
                map.put("model", GL.u!!.model!!)
                map.put("year", GL.u!!.year!!)
                map.put("color", GL.u!!.color!!)
                map.put("interiorColor", GL.u!!.interiorColor!!)
                map.put("vehicleThumbnail", GL.u!!.vehicleThumbnail!!)

                map.put("licenseNumber", GL.u!!.licenseNumber!!)
                map.put("vehicleValidType", GL.u!!.vehicleValidType!!)
                map.put("issuedDate", GL.u!!.issuedDate!!)
                map.put("expiryDate", GL.u!!.expiryDate!!)
                map.put("licenseThumbnail", GL.u!!.licenseThumbnail!!)

                map.put("refOne", GL.u!!.refOne!!)
                map.put("refTwo", GL.u!!.refTwo!!)
                map.put("refThree", GL.u!!.refThree!!)
                map.put("refFour", GL.u!!.refFour!!)
                map.put("refFive", GL.u!!.refFive!!)

                map.put("offerOne", offer1!!)
                map.put("offerTwo", offer2!!)
                map.put("offerThree", offer3!!)

                mProgressDialog!!.setTitle("Processing")
                mProgressDialog!!.setMessage("Please wait...")
                mProgressDialog!!.setCanceledOnTouchOutside(false)
                mProgressDialog!!.show()

                val d = Driver(
                    k, GL.u!!.uid, GL.u!!.name, GL.u!!.email, GL.u!!.password, GL.u!!.phone,
                    GL.u!!.nic, GL.u!!.thumbnail, GL.u!!.userType, GL.u!!.brand, GL.u!!.model, GL.u!!.year,
                    GL.u!!.color, GL.u!!.interiorColor, GL.u!!.vehicleThumbnail, GL.u!!.licenseNumber,
                    GL.u!!.vehicleValidType, GL.u!!.issuedDate, GL.u!!.expiryDate, GL.u!!.licenseThumbnail,
                    GL.u!!.refOne, GL.u!!.refTwo, GL.u!!.refThree, GL.u!!.refFour, GL.u!!.refFive,
                    offer1, offer2, offer3
                )

                refer.setValue(d).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        ref1.setValue(map).addOnCompleteListener { task ->
                            if (task.isSuccessful) {

                                mProgressDialog!!.dismiss()

                                GL.u!!.uid = GL.u!!.uid!!
                                GL.u!!.name = GL.u!!.name!!
                                GL.u!!.email = GL.u!!.email!!
                                GL.u!!.password = GL.u!!.password!!
                                GL.u!!.phone = GL.u!!.phone!!
                                GL.u!!.nic = GL.u!!.nic!!
                                GL.u!!.thumbnail = GL.u!!.thumbnail!!
                                GL.u!!.userType = "driver"

                                GL.u!!.brand = GL.u!!.brand!!
                                GL.u!!.model = GL.u!!.model!!
                                GL.u!!.year = GL.u!!.year!!
                                GL.u!!.color = GL.u!!.color!!
                                GL.u!!.interiorColor = GL.u!!.interiorColor!!
                                GL.u!!.vehicleThumbnail = GL.u!!.vehicleThumbnail!!

                                GL.u!!.licenseNumber = GL.u!!.licenseNumber
                                GL.u!!.vehicleValidType = GL.u!!.vehicleValidType
                                GL.u!!.issuedDate = GL.u!!.issuedDate
                                GL.u!!.expiryDate = GL.u!!.expiryDate
                                GL.u!!.licenseThumbnail = GL.u!!.licenseThumbnail

                                GL.u!!.refOne = GL.u!!.refOne
                                GL.u!!.refTwo = GL.u!!.refTwo
                                GL.u!!.refThree = GL.u!!.refThree
                                GL.u!!.refFour = GL.u!!.refFour
                                GL.u!!.refFive = GL.u!!.refFive

                                GL.u!!.offerOne = offer1
                                GL.u!!.offerTwo = offer2
                                GL.u!!.offerThree = offer3

                                val intent = Intent(this, MainActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                startActivity(intent)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
