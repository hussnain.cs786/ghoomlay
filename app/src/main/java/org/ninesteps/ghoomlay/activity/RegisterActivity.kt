package org.ninesteps.ghoomlay.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.User
import org.ninesteps.sociotravel.helper.InputValidation
import java.io.ByteArrayOutputStream
import java.io.IOException

class RegisterActivity : AppCompatActivity(), View.OnClickListener {

    private val GALLERY = 100
    private val CAMERA = 10
    internal var encodedImage = "thumbnail"

    internal var inputValidation_: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    internal var n: String? = null
    internal var e_: String? = null
    internal var p_: String? = null
    internal var cp: String? = null
    internal var ph: String? = null
    internal var nic: String? = null

    internal var userType: String? = null

    internal var thumbnail = "empty"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val register = findViewById<Button>(R.id.register)
        val browseImage = findViewById<RelativeLayout>(R.id.browse_image)

        if (intent.getStringExtra(GL.USERTTYPE) != null)
            userType = intent.getStringExtra(GL.USERTTYPE)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = "Register"

        register.setOnClickListener(this)
        browseImage.setOnClickListener(this)

        inputValidation_ = InputValidation(this)
        mProgress = ProgressDialog(this)
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.browse_image -> {
                imageDialog()
            }

            R.id.register -> {
                n = name.text.toString()
                e_ = reg_email.text.toString()
                p_ = reg_pass.getText().toString()
                ph = reg_phone.getText().toString()
                cp = reg_cp.text.toString()
                nic = reg_cnic.text.toString()

                if (encodedImage.equals("thumbnail")) {
                    Toast.makeText(this, "Please select image", Toast.LENGTH_LONG).show()
                } else
                    if (!inputValidation_!!.isInputEditTextFilled(
                            name,
                            name_layout,
                            getString(R.string.error_message_name)
                        )
                    ) {
                        return
                    } else
                        if (!inputValidation_!!.isInputEditTextFilled(
                                reg_email,
                                email_layout,
                                getString(R.string.error_message_email)
                            )
                        ) {
                            return
                        } else
                            if (!inputValidation_!!.isInputEditTextEmail(
                                    reg_email,
                                    email_layout,
                                    getString(R.string.error_message_email)
                                )
                            ) {
                                return
                            } else
                                if (!inputValidation_!!.isInputEditTextFilled(
                                        reg_pass,
                                        pass_layout,
                                        getString(R.string.error_message_password)
                                    )
                                ) {
                                    return
                                } else
                                    if (!inputValidation_!!.isInputEditTextMatches(
                                            reg_pass, reg_cp,
                                            cp_layout, getString(R.string.error_password_match)
                                        )
                                    ) {
                                        return
                                    } else
                                        if (!inputValidation_!!.isInputEditTextFilled(
                                                reg_phone,
                                                phone_layout,
                                                getString(R.string.error_phone_number)
                                            )
                                        ) {
                                            return
                                        } else if (!inputValidation_!!.isInputEditTextFilled(
                                                reg_cnic,
                                                nic_layout,
                                                getString(R.string.error_nic)
                                            )
                                        ) run { return }
                                        else {

                                            mProgress!!.setTitle("Signing up")
                                            mProgress!!.setMessage("Please wait while we check your credentials.")
                                            mProgress!!.setCanceledOnTouchOutside(false)
                                            mProgress!!.show()

                                            GL.getAuth().createUserWithEmailAndPassword(e_!!, p_!!)
                                                .addOnCompleteListener(this) { task ->
                                                    if (task.isSuccessful) {
                                                        Log.d("Login", "signInWithEmail:success")
                                                        val user = User(
                                                            GL.getUserId(),
                                                            n!!,
                                                            e_!!,
                                                            p_!!,
                                                            ph!!,
                                                            nic!!,
                                                            encodedImage,
                                                            "user",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default",
                                                            "default"
                                                        )
                                                        GL.getDatabaseRef().child("users")
                                                            .child(GL.getUserId()).setValue(user)
                                                        GL.u = user
                                                        mProgress!!.dismiss()
                                                        if (userType.equals("user")){
                                                            val intent = Intent(this, MainActivity::class.java)
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                            startActivity(intent)
                                                        }
                                                        else if (userType.equals("driver")) {

                                                            val intent = Intent(this, VehicleActivity::class.java)
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                            startActivity(intent)
                                                        }

                                                    } else {
                                                        mProgress!!.dismiss()
                                                        // If sign in fails, display a message to the user.
                                                        Log.w("Login", "signInWithEmail:failure", task.getException())
                                                        Toast.makeText(
                                                            this,
                                                            task.exception!!.message,
                                                            Toast.LENGTH_LONG
                                                        ).show()
                                                    }
                                                }


                                        }
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        user_thumbnail.setImageBitmap(bit)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }
}
