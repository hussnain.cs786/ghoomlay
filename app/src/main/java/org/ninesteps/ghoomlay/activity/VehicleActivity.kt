package org.ninesteps.ghoomlay.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_vehicle.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.HashMap

class VehicleActivity : AppCompatActivity() {

    private val GALLERY = 100
    private val CAMERA = 10
    internal var encodedImage = "thumbnail"

    var b: String? = null
    var m: String? = null
    var y: String? = null
    var c: String? = null
    var ic: String? = null
    var mProgressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle)
        mProgressDialog = ProgressDialog(this)
        title = "Vehicle Details"

        vehicleImage.setOnClickListener {
            imageDialog()
        }
        vehicle_next.setOnClickListener {
            b = vehicle_brand.text.toString()
            m = vehicle_model.text.toString()
            y = vehicle_year.text.toString()
            c = vehicle_color.text.toString()
            ic = vehicle_interior_color.text.toString()

            if (encodedImage.equals("thumbnail")){
                Toast.makeText(this,"Please select image",Toast.LENGTH_LONG).show()
            }else
            if (b!!.isEmpty()
                || m!!.isEmpty()
                || y!!.isEmpty()
                || c!!.isEmpty()
                || ic!!.isEmpty()) {
                Toast.makeText(this, "All fields are required!", Toast.LENGTH_LONG).show()
            } else
                if (Integer.parseInt(y!!) > 2018)
                    Toast.makeText(this, "Year can't be greater than 2018.", Toast.LENGTH_LONG).show()
                else {

                    val ref1 = GL.getDatabaseRef().child("users").child(GL.getUserId())

                    val map = HashMap<String, String>()

                    map.put("uid", GL.u!!.uid!!)
                    map.put("name", GL.u!!.name!!)
                    map.put("email", GL.u!!.email!!)
                    map.put("password", GL.u!!.password!!)
                    map.put("phone", GL.u!!.phone!!)
                    map.put("nic", GL.u!!.nic!!)
                    map.put("thumbnail", GL.u!!.thumbnail!!)
                    map.put("userType", "user")

                    map.put("brand", b!!)
                    map.put("model", m!!)
                    map.put("year", y!!)
                    map.put("color", c!!)
                    map.put("interiorColor", ic!!)
                    map.put("vehicleThumbnail", encodedImage)

                    map.put("licenseNumber", GL.u!!.licenseNumber!!)
                    map.put("vehicleValidType", GL.u!!.vehicleValidType!!)
                    map.put("issuedDate", GL.u!!.issuedDate!!)
                    map.put("expiryDate", GL.u!!.expiryDate!!)
                    map.put("licenseThumbnail", GL.u!!.licenseThumbnail!!)

                    map.put("refOne", GL.u!!.refOne!!)
                    map.put("refTwo", GL.u!!.refTwo!!)
                    map.put("refThree", GL.u!!.refThree!!)
                    map.put("refFour", GL.u!!.refFour!!)
                    map.put("refFive", GL.u!!.refFive!!)

                    map.put("offerOne", GL.u!!.offerOne!!)
                    map.put("offerTwo", GL.u!!.offerTwo!!)
                    map.put("offerThree", GL.u!!.offerThree!!)

                    mProgressDialog!!.setTitle("Processing")
                    mProgressDialog!!.setMessage("Please wait...")
                    mProgressDialog!!.setCanceledOnTouchOutside(false)
                    mProgressDialog!!.show()

                    ref1.setValue(map).addOnCompleteListener { task ->
                        if (task.isSuccessful) {

                            mProgressDialog!!.dismiss()

                            GL.u!!.uid = GL.u!!.uid!!
                            GL.u!!.name = GL.u!!.name!!
                            GL.u!!.email = GL.u!!.email!!
                            GL.u!!.password = GL.u!!.password!!
                            GL.u!!.phone = GL.u!!.phone!!
                            GL.u!!.nic = GL.u!!.nic!!
                            GL.u!!.thumbnail = GL.u!!.thumbnail!!
                            GL.u!!.userType = "driver"

                            GL.u!!.brand = b!!
                            GL.u!!.model = m!!
                            GL.u!!.year = y!!
                            GL.u!!.color = c!!
                            GL.u!!.interiorColor = ic!!
                            GL.u!!.vehicleThumbnail = encodedImage

                            GL.u!!.licenseNumber = GL.u!!.licenseNumber!!
                            GL.u!!.vehicleValidType = GL.u!!.vehicleValidType!!
                            GL.u!!.issuedDate = GL.u!!.issuedDate!!
                            GL.u!!.expiryDate = GL.u!!.expiryDate!!
                            GL.u!!.licenseThumbnail = GL.u!!.licenseThumbnail!!

                            GL.u!!.refOne = GL.u!!.refOne
                            GL.u!!.refTwo = GL.u!!.refTwo
                            GL.u!!.refThree = GL.u!!.refThree
                            GL.u!!.refFour = GL.u!!.refFour
                            GL.u!!.refFive = GL.u!!.refFive

                            GL.u!!.offerOne = GL.u!!.offerOne
                            GL.u!!.offerTwo = GL.u!!.offerTwo
                            GL.u!!.offerThree = GL.u!!.offerThree

                            startActivity(Intent(this,LicenseActivity::class.java))
                        }
                    }
                }
        }
    }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        vehicle_thumbnail.setImageBitmap(bit)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }
}
