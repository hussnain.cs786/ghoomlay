package org.ninesteps.ghoomlay.activity.admin

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_add_point.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.Point
import org.ninesteps.sociotravel.helper.InputValidation
import java.io.ByteArrayOutputStream
import java.io.IOException
//import com.google.android.libraries.places.compat.Place;

class AddPoint : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    internal lateinit var arrayAdapter: ArrayAdapter<*>
    internal var filter = arrayOf("Hotels", "Sightseeing", "Nightlife", "Eats and Drinks")

//    var address = "default"
//    var longitude = "default"
//    var latitude = "default"

    var address = "Lahore"
    var latitude = 31.4826352
    var longitude = 74.054189

    var refrence = "default"
    var tagone = "tagone"
    var tagtwo = "tagtwo"
    var tagthree = "tagthree"
    internal var encodedImage = "thumbnail"
    var type = "type"
    var phone = "default"

    internal var name: String? = null
    internal var city: String? = null
    internal var distance: String? = null
    internal var opening: String? = null
    internal var closing: String? = null

    private val GALLERY = 100
    private val CAMERA = 10

    internal var inputValidation_: InputValidation? = null
    private var mProgress: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_point)

//        if (!Places.isInitialized()) {
//            Places.initialize(applicationContext, resources.getString(R.string.google_api_key));
//        }

        title = resources.getString(R.string.add_point)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        city = intent.getStringExtra("CITY")

        inputValidation_ = InputValidation(this)
        mProgress = ProgressDialog(this)

        spinner_ptype.onItemSelectedListener = this
        arrayAdapter = ArrayAdapter(this, R.layout.spinner_list, filter)
        arrayAdapter.setDropDownViewResource(R.layout.spinner_item)
        spinner_ptype.setAdapter(arrayAdapter)

        val autocompleteFragment =
            fragmentManager.findFragmentById(R.id.p_autocomplete_fragment) as PlaceAutocompleteFragment

        /*AutocompleteFilter filter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(filter);*/
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onError(status: Status?) {
                Toast.makeText(this@AddPoint, status.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onPlaceSelected(place: Place) {
                place_address.text = place.name.toString()
                Log.e("NAME:", place.name.toString())
                Log.e("LONGITUDE", place.latLng.longitude.toString())
                Log.e("LATITUDE", place.latLng.latitude.toString())

                address = place.name.toString()
                longitude = place.latLng.longitude
                latitude = place.latLng.latitude
            }
        })

        browse_pimage.setOnClickListener {
            imageDialog()
        }
        add_point.setOnClickListener {

            name = point_name.text.toString()
            refrence = point_ref.text.toString()
            distance = point_distance.text.toString()
            phone = point_phone.text.toString()
            opening = point_opening.text.toString()
            closing = point_closing.text.toString()
            tagone = tag01.text.toString()
            tagtwo = tag02.text.toString()
            tagthree = tag03.text.toString()

            if (encodedImage.equals("thumbnail")) {
                Toast.makeText(this, "Please select image", Toast.LENGTH_LONG).show()
            } else if (address.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else if (longitude.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else if (latitude.equals("default")) {
                Toast.makeText(this, "Please add location", Toast.LENGTH_LONG).show()
            } else
                if (!inputValidation_!!.isInputEditTextFilled(
                        point_name,
                        pname_layout,
                        "Enter Point name"
                    )
                ) {
                    return@setOnClickListener
                } else
                    if (!inputValidation_!!.isInputEditTextFilled(
                            point_distance,
                            pdistance_layout,
                            "Enter Point Distance"
                        )
                    ) {
                        return@setOnClickListener
                    } else
                        if (!inputValidation_!!.isInputEditTextFilled(
                                point_opening,
                                popentime_layout,
                                "Enter Opening time"
                            )
                        ) {
                            return@setOnClickListener
                        } else
                            if (!inputValidation_!!.isInputEditTextFilled(
                                    point_closing,
                                    pclosetime_layout,
                                    "Enter Closing time"
                                )
                            ) {
                                return@setOnClickListener
                            } else {
                                //...
                                mProgress!!.setTitle("Adding...")
                                mProgress!!.setMessage("Please wait while we check your credentials.")
                                mProgress!!.setCanceledOnTouchOutside(false)
                                mProgress!!.show()

                                val point = Point(
                                    name,
                                    encodedImage,
                                    type,
                                    city,
                                    refrence,
                                    distance,
                                    address,
                                    longitude,
                                    latitude,
                                    opening,
                                    closing,
                                    phone,
                                    tagone,
                                    tagtwo,
                                    tagthree
                                )
                                val key = GL.getDatabaseRef().child("points").child(city).push().getKey()
                                GL.getDatabaseRef().child("points")
                                    .child(city)
                                    .child(key)
                                    .setValue(point)
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            mProgress!!.dismiss()
                                            finish()
                                            Toast.makeText(this, "Data added successfully!", Toast.LENGTH_LONG).show()
                                        } else {
                                            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                                        }
                                    }
                            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
        type = adapterView!!.getItemAtPosition(position).toString()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun imageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Add Photo!")

        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {

                val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(takePicture, CAMERA)

            } else if (options[item] == "Choose from Gallery") {

                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, GALLERY)

            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }

        builder.show()
    }

    //Get images from gallery and camera
    fun getImage(data: Intent, code: Int) {
        val contentURI: Uri?
        val bundle: Bundle?

        var bit: Bitmap? = null
        if (code == GALLERY) {
            contentURI = data.data
            try {
                bit = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else if (code == CAMERA) {
            bundle = data.extras
            if (bundle!!.get("data") != null)
                bit = bundle.get("data") as Bitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bit!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        point_thumbnail.setImageBitmap(bit)
        encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == GALLERY) run {
            if (data != null) {
                getImage(data, requestCode)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA) {
            if (data != null) {
                getImage(data, requestCode)
            }
        }
    }
}
