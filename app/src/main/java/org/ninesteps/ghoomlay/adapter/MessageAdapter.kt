package org.ninesteps.ghoomlay.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mikhaellopez.circularimageview.CircularImageView
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.model.Conversation
import java.text.SimpleDateFormat
import java.util.*

class MessageAdapter(
    private val context: Context,
    private val conversation: Conversation,
    private val bitmapAvataFriend: Bitmap,
    private val bitmapAvataUser: Bitmap
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            GL.VIEW_TYPE_FRIEND_MESSAGE -> FriendViewHolder(
                inflater.inflate(
                    R.layout.rc_item_message_friend,
                    parent,
                    false
                )
            )
            else -> UserViewHolder(inflater.inflate(R.layout.rc_item_message_user, parent, false))
        }
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is FriendViewHolder) {
            holder.txtContent.text = conversation.listMessageData[position].text
            holder.avata.setImageBitmap(bitmapAvataFriend)

            val time =
                SimpleDateFormat("EEE, d MMM yyyy")
                    .format(Date(conversation.listMessageData[position].timestamp))
            val today = SimpleDateFormat("EEE, d MMM yyyy").format(Date(System.currentTimeMillis()))

            if (today == time) {
                holder.timestamp.text =
                    SimpleDateFormat("hh:mm a").format(Date(conversation.listMessageData[position].timestamp))
            } else {
                holder.timestamp.text = SimpleDateFormat("EEE, d MMM yyyy HH:mm a").format(
                    Date(
                        conversation.listMessageData.get(position).timestamp
                    )
                )

            }
        } else
            if (holder is UserViewHolder) {
            holder.txtContent.text = conversation.listMessageData[position].text
            holder.avata.setImageBitmap(bitmapAvataUser)

            val time =
                SimpleDateFormat("EEE, d MMM yyyy").format(Date(conversation.listMessageData[position].timestamp))
            val today = SimpleDateFormat("EEE, d MMM yyyy").format(Date(System.currentTimeMillis()))

            if (today == time) {
                holder.timestamp.text =
                    SimpleDateFormat("hh:mm a").format(Date(conversation.listMessageData[position].timestamp))
            } else {
                holder.timestamp.text = SimpleDateFormat("EEE, d MMM yyyy HH:mm a").format(
                    Date(
                        conversation.listMessageData.get(position).timestamp
                    )
                )

            }

        }
    }

    override fun getItemCount(): Int = conversation.getListMessageData().size

    inner class UserViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val txtContent = mView.findViewById<View>(R.id.textContentUser) as TextView
        val avata = mView.findViewById<View>(R.id.imageContentUser) as CircularImageView
        val timestamp = mView.findViewById(R.id.timestamp) as TextView
    }

    inner class FriendViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val txtContent = itemView.findViewById(R.id.textContentFriend) as TextView
        val avata = itemView.findViewById<CircularImageView>(R.id.imageContentFriend)
        val timestamp = itemView.findViewById(R.id.timestamp) as TextView
    }

    override fun getItemViewType(position: Int): Int {
        return if (conversation.listMessageData.get(position).idSender == GL.getUserId())
            GL.VIEW_TYPE_USER_MESSAGE
        else
            GL.VIEW_TYPE_FRIEND_MESSAGE
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }

}
