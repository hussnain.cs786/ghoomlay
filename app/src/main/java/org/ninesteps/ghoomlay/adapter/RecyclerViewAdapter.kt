package org.ninesteps.ghoomlay.adapter

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_all_drivers.view.*
import kotlinx.android.synthetic.main.activity_point.view.*
import kotlinx.android.synthetic.main.chat.view.*
import kotlinx.android.synthetic.main.content_main.view.*
import kotlinx.android.synthetic.main.driver_request.view.*
import org.ninesteps.ghoomlay.GL
import org.ninesteps.ghoomlay.R
import org.ninesteps.ghoomlay.activity.*
import org.ninesteps.ghoomlay.helper.ExtendedDataHolder
import org.ninesteps.ghoomlay.model.Chat
import org.ninesteps.ghoomlay.model.City
import org.ninesteps.ghoomlay.model.Driver
import org.ninesteps.ghoomlay.model.Point

class RecyclerViewAdapter(
    private var mValues: MutableList<Any>,
    private val mViewType: Int,
    private val mActivity: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        //            val item = v.tag as DummyItem
//            // Notify the active callbacks interface (the activity, if the fragment is attached to
//            // one) that an item has been selected.
//            mListener?.onListFragmentInteraction(item)
    }

//    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (mViewType) {
            VIEW_TYPE_CITIES -> CityViewHolder(inflater.inflate(R.layout.content_main, parent, false))
            VIEW_TYPE_POINTS -> PointsViewHolder(inflater.inflate(R.layout.activity_point, parent, false))
            VIEW_TYPE_DRIVERS -> DriverViewHolder(inflater.inflate(R.layout.activity_all_drivers, parent, false))
            VIEW_TYPE_NOTIFICATIONS -> NotificationViewHolder(inflater.inflate(R.layout.chat_list, parent, false))
            VIEW_TYPE_CHAT -> ChatViewHolder(inflater.inflate(R.layout.chat, parent, false))
            else -> RequestsViewHolder(inflater.inflate(R.layout.driver_request, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = mValues[position]

        when (mViewType) {
            VIEW_TYPE_CITIES -> {
                val c = item as City
                val ch = holder as CityViewHolder

                ch.mName.text = c.name
                ch.mDesc.text = c.description
                ch.mProvince.text = c.province

                val bitmap = stringToBitMap(c.thumbnail1.toString())
                ch.mImage.setImageBitmap(bitmap)

                ch.itemView.setOnClickListener {
                    val extras = ExtendedDataHolder.getInstance()
                    extras.putExtra("extra", ByteArray(1024 * 1024))
                    extras.putExtra(GL.MODEL_CITY, c)
                    val intent = Intent(ch.mView.context, CityDetail::class.java)
                    mActivity.startActivity(intent)

//                    // Get the transition name from the string
//                    val transitionName = ch.mView.context.getString(R.string.simple_activity_transition)
//
//                    val options =
//                        ActivityOptionsCompat.makeSceneTransitionAnimation(
//                            mActivity,
//                            ch.mImage, // Starting view
//                            transitionName    // The String
//                        )
//                    //Start the Intent
//                    ActivityCompat.startActivity(mActivity, intent, options.toBundle())

                }
            }

            VIEW_TYPE_POINTS -> {
                val p = item as Point
                val ph = holder as PointsViewHolder

                Log.e("TAGONE", p.tagone)
                Log.e("TAGTWO", p.tagtwo)
                Log.e("TAGTHREE", p.tagthree)

                if (p.tagone!!.isEmpty()) {
                    ph.mTag1.visibility = View.GONE
                } else {
                    ph.mTag1.visibility = View.VISIBLE
                    ph.mTag1.text = p.tagone
                }
                if (p.tagtwo!!.isEmpty()) {
                    ph.mTag2.visibility = View.GONE
                } else {
                    ph.mTag2.visibility = View.VISIBLE
                    ph.mTag2.text = p.tagtwo
                }
                if (p.tagthree!!.isEmpty()) {
                    ph.mTag3.visibility = View.GONE
                } else {
                    ph.mTag3.visibility = View.VISIBLE
                    ph.mTag3.text = p.tagthree
                }

                ph.mName.text = p.name
                ph.mDistance.text = p.distance
                ph.mRefrence.text = p.refrence

                val bitmap = stringToBitMap(p.thumbnail.toString())
                ph.mImage.setImageBitmap(bitmap)

                ph.mView.setOnClickListener {
                    val intent = Intent(ph.mView.context, PointDetail::class.java)
                    intent.putExtra(GL.MODEL_POINTS, p)

                    // Get the transition name from the string
                    val transitionName = ph.mView.context.getString(R.string.simple_activity_transition)

                    val options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                            mActivity,
                            ph.mImage, // Starting view
                            transitionName    // The String
                        )
                    //Start the Intent
                    ActivityCompat.startActivity(mActivity, intent, options.toBundle())
                }

            }

            VIEW_TYPE_DRIVERS -> {
                val model = item as Driver
                val dh = holder as DriverViewHolder

                dh.mName.text = model.name
                val bitmap = stringToBitMap(model.thumbnail.toString())
                dh.mImage.setImageBitmap(bitmap)

                dh.mView.setOnClickListener {

                    val extras = ExtendedDataHolder.getInstance()
                    extras.putExtra("extra", ByteArray(1024 * 1024))
                    extras.putExtra("DRIVER-MODEL", model)

                    val i = Intent(dh.mView.context, DriverDetail::class.java)
                    i.putExtra("TYPE", "accept")
                    mActivity.startActivity(i)
                }

                dh.mButton.setOnClickListener {

                    if (GL.isSignedIn()) {
                        dh.mProgress.setTitle("Processing...")
                        dh.mProgress.setMessage("Please wait we are creating your chat.")
                        dh.mProgress.setCanceledOnTouchOutside(false)
                        dh.mProgress.show()

                        val roomid = model.uid + GL.getUserId()
                        val senderChat = Chat(
                            model.name,
                            model.thumbnail,
                            model.uid,
                            GL.u!!.name,
                            GL.u!!.thumbnail,
                            GL.getUserId(),
                            roomid
                        )
                        val receiverChat = Chat(
                            GL.u!!.name,
                            GL.u!!.thumbnail,
                            GL.getUserId(),
                            model.name,
                            model.thumbnail,
                            model.uid,
                            roomid
                        )
                        GL.getDatabaseRef().child("all-chats").child(GL.getUserId()).child(model.uid)
                            .setValue(senderChat)
                        GL.getDatabaseRef().child("all-chats").child(model.uid).child(GL.getUserId())
                            .setValue(receiverChat)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    dh.mProgress.dismiss()

                                    val extras = ExtendedDataHolder.getInstance()
                                    extras.putExtra("extra", ByteArray(1024 * 1024))
                                    extras.putExtra("CHAT-IMAGE", model.thumbnail)

                                    val i = Intent(dh.mView.context, ChatActivity::class.java)
                                    i.putExtra("CHAT-NAME", model.name)
                                    i.putExtra("CHAT-ID", model.uid)
                                    i.putExtra("ROOM-ID", roomid)
                                    mActivity.startActivity(i)
                                } else {
                                    dh.mProgress.dismiss()
                                    Toast.makeText(
                                        mActivity,
                                        "Something went wrong. Please try again later.",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                    } else {
                        mActivity.startActivity(Intent(mActivity, LoginActivity::class.java))
                        // close this activity
//                        mActivity.finish()
                    }
                }
            }

            VIEW_TYPE_NOTIFICATIONS -> {
//                val f = item as FollowUs
//                val fh = holder as FollowUsViewHolder
//
//                fh.mName.text = f.name
//                fh.mImage.setImageResource(f.image)
//
//                fh.mView.setOnClickListener {
//                    NS.getInstance().showToast(f.name)
//                }
            }

            VIEW_TYPE_CHAT -> {
                val c = item as Chat
                val ch = holder as ChatViewHolder

//                if (GL.u!!.userType == "user") {
//                    ch.mName.text = c.rname
//                    val bitmap = stringToBitMap(c.rthumbnail.toString())
//                    ch.mImage.setImageBitmap(bitmap)
//                }else{
                ch.mName.text = c.sname
                val bitmap = stringToBitMap(c.sthumbnail.toString())
                ch.mImage.setImageBitmap(bitmap)
//                }

                Log.e("NAME1: ", c.rname)
                Log.e("NAME2: ", c.sname)

                ch.mView.setOnClickListener {

                    val extras = ExtendedDataHolder.getInstance()
                    extras.putExtra("extra", ByteArray(1024 * 1024))
                    extras.putExtra("CHAT-IMAGE", c.sthumbnail)
                    val i = Intent(ch.mView.context, ChatActivity::class.java)
                    i.putExtra("CHAT-NAME", c.sname)
                    i.putExtra("CHAT-ID", c.sid)
                    i.putExtra("ROOM-ID", c.roomid)
                    mActivity.startActivity(i)
                }
            }

            else -> {
                val model = item as Driver
                val rh = holder as RequestsViewHolder

                rh.mName.text = model.name
                val bitmap = stringToBitMap(model.thumbnail.toString())
                rh.mImage.setImageBitmap(bitmap)

                rh.mView.setOnClickListener {

                    val extras = ExtendedDataHolder.getInstance()
                    extras.putExtra("extra", ByteArray(1024 * 1024))
                    extras.putExtra("DR-MODEL", model)

                    val i = Intent(rh.mView.context, DriverDetail::class.java)
                    i.putExtra("TYPE", "request")
                    mActivity.startActivity(i)
                }

                rh.mButton.setOnClickListener {
                    val k = GL.getDatabaseRef().child("all-drivers").push().key
                    val d = Driver(
                        k,
                        model.uid, model.name, model.email, model.password, model.phone, model.nic, model.thumbnail,
                        model.userType, model.brand, model.model, model.year, model.color, model.interiorColor,
                        model.vehicleThumbnail, model.licenseNumber, model.vehicleValidType, model.issuedDate,
                        model.expiryDate, model.licenseThumbnail,
                        model.refOne, model.refTwo, model.refThree, model.refFour, model.refFive,
                        model.offerOne, model.offerTwo, model.offerThree
                    )

                    val refer = GL.getDatabaseRef().child("all-drivers").child(k)
                    refer.setValue(d).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(rh.mView.context, "Request Accepted", Toast.LENGTH_LONG).show()
                            GL.getDatabaseRef().child("pending-drivers").child(model.key).removeValue()
                        } else {

                        }
                    }


                }
            }
        }

    }

    override fun getItemCount(): Int = mValues.size

    inner class CityViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.cityname
        val mDesc: TextView = mView.citydesc
        val mProvince: TextView = mView.cityprovince
        val mImage: ImageView = mView.citythumbnail
        var mProgress: ProgressDialog = ProgressDialog(mActivity)
    }

    inner class PointsViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.pointname
        val mDistance: TextView = mView.pointdistance
        val mRefrence: TextView = mView.pointref
        val mImage: ImageView = mView.pointthumbnail
        val mTag1: TextView = mView.tag1
        val mTag2: TextView = mView.tag2
        val mTag3: TextView = mView.tag3
    }

    inner class DriverViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.driver_name
        val mImage: ImageView = mView.driver_image
        val mButton = mView.send_message
        var mProgress: ProgressDialog = ProgressDialog(mActivity)
    }

    inner class NotificationViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
//        val mName: TextView = mView.follow_name
//        val mImage: ImageView = mView.follow_image
    }

    inner class ChatViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.chat_name
        val mImage: ImageView = mView.chat_image
        var mProgress: ProgressDialog = ProgressDialog(mActivity)
    }

    inner class RequestsViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mName: TextView = mView.dr_name
        val mImage: ImageView = mView.dr_image
        val mButton = mView.accept_driver
        var mProgress: ProgressDialog = ProgressDialog(mActivity)
    }

    companion object {
        val VIEW_TYPE_CITIES = 0
        val VIEW_TYPE_POINTS = 1
        val VIEW_TYPE_DRIVERS = 2
        val VIEW_TYPE_NOTIFICATIONS = 3
        val VIEW_TYPE_CHAT = 4
        val VIEW_TYPE_SENDER = 5
        val VIEW_TYPE_RECEIVER = 6
        val VIEW_TYPE_REQUESTS = 7
    }

/*

    //load fragment on recyclerView OnClickListner
    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    interface OnItemClickListener {

        fun onItemClick(model: Any, position: Int, itemView: View)
    }
*/

    fun setFilter(newList: ArrayList<Any>) {
        mValues = ArrayList()
        (mValues as ArrayList<Any>).addAll(newList)
        notifyDataSetChanged()
    }

    fun stringToBitMap(encodedString: String): Bitmap? {
        try {
            val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            return null
        }

    }

}
