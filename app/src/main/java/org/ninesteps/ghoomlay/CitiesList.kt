package org.ninesteps.ghoomlay

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_cities_list.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main_list.*
import org.ninesteps.ghoomlay.activity.ChatList
import org.ninesteps.ghoomlay.activity.LoginActivity
import org.ninesteps.ghoomlay.activity.admin.AddCity
import org.ninesteps.ghoomlay.adapter.RecyclerViewAdapter
import org.ninesteps.ghoomlay.model.City

class CitiesList : AppCompatActivity(), SearchView.OnQueryTextListener {

    private var mListType = RecyclerViewAdapter.VIEW_TYPE_CITIES
    private var mAdapter: RecyclerViewAdapter? = null
    var mList: MutableList<Any> = ArrayList()

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cities_list)
        title = "All cities"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        addcity.setOnClickListener {
            startActivity(Intent(this@CitiesList, AddCity::class.java))
        }

        if (GL.isSignedIn()) {
            if (GL.u!!.userType.equals("admin")) {
                addcity.visibility = View.VISIBLE
            } else {
                addcity.visibility = View.GONE
            }
        }else{
            addcity.visibility = View.GONE
        }

        GL.getDatabaseRef().child("cities").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("CITIES", "OK")
                mList.clear()
                for (dataSnapshot1 in dataSnapshot.children) {

                    val model = dataSnapshot1.getValue(City::class.java)!!
                    mList.add(0, model)

                }
                mAdapter = RecyclerViewAdapter(mList, mListType, this@CitiesList)
                city_list.adapter = mAdapter
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(menuItem) as SearchView
        searchView.queryHint = getString(R.string.search_cities)
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_chat -> {
                if (GL.isSignedIn()) {
                    startActivity(Intent(this@CitiesList, ChatList::class.java))
                } else {
                    startActivity(Intent(this@CitiesList, LoginActivity::class.java))
                    // close this activity
//                    finish()
                }
                true
            }
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val action_map = menu!!.findItem(R.id.action_map)
        val action_chat = menu.findItem(R.id.action_chat)
        val action_search = menu.findItem(R.id.action_search)
        val action_add = menu.findItem(R.id.action_add)

        action_add.isVisible = false
        action_map.isVisible = false

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        when (mListType) {
            RecyclerViewAdapter.VIEW_TYPE_CITIES -> {

                val newList = ArrayList<Any>()
                for (model in mList) {
                    model as City
                    val name = model.name!!.toLowerCase()
                    val province = model.province!!.toLowerCase()
                    if (name.contains(newText!!.toLowerCase())
                        or province.contains(newText.toLowerCase())
                    )
                        newList.add(model)
                }
                mAdapter!!.setFilter(newList)
            }
        }
        return true
    }
}
