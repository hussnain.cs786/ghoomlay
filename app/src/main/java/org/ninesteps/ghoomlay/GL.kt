package org.ninesteps.ghoomlay

import android.support.multidex.MultiDexApplication
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import org.ninesteps.ghoomlay.model.User

class GL : MultiDexApplication() {
    companion object {
        var u: User? = null
        var USERTTYPE = "user-type"
        var MODEL_CITY = "model-city"
        var MODEL_POINTS = "model-point"
        var POINT_TYPE = "point-type"
        var POINT_CITY = "point-city"
        var DRIVER_STATUS = "driver-status"
        var DRIVER_CITY = "driver-city"

        //For mapActivity
        var MAP_STATUS = "map-status"
        var MAP_TITLE = "map-title"
        var MAP_LONGITUDE = "map-longitude"
        var MAP_LATITUDE = "map-latitude"
        var MAP_LIST = "map-list"

        val VIEW_TYPE_USER_MESSAGE = 10
        val VIEW_TYPE_FRIEND_MESSAGE = 11

        fun getAuth(): FirebaseAuth {
            return FirebaseAuth.getInstance()
        }

        fun getUserId(): String {
            return FirebaseAuth.getInstance().currentUser!!.uid
        }

        fun isSignedIn(): Boolean {
            return FirebaseAuth.getInstance().currentUser != null
        }

        fun getDatabaseRef(): DatabaseReference {
            return FirebaseDatabase.getInstance().reference
        }

        fun getStorageRef(): StorageReference {
            return FirebaseStorage.getInstance().reference
        }

        fun getUploadTask(task: UploadTask): UploadTask {
            return task
        }
    }
}
