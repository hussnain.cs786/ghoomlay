package org.ninesteps.ghoomlay.model;



public class Message {
    public String idSender;
    public String text;
    public long timestamp;

    public Message() {
    }

    public Message(String idSender, String text, long timestamp) {
        this.idSender = idSender;
        this.text = text;
        this.timestamp = timestamp;
    }
}