package org.ninesteps.ghoomlay.model

import java.io.Serializable

class Driver : Serializable {
    //personal detail
    var key: String? = null
    var uid: String? = null
    var name: String? = null
    var email: String? = null
    var password: String? = null
    var phone: String? = null
    var nic: String? = null
    var thumbnail: String? = null
    var userType: String? = null

    //vehicle detail
    var brand: String? = null
    var model: String? = null
    var year: String? = null
    var color: String? = null
    var interiorColor: String? = null
    var vehicleThumbnail: String? = null

    //license detail
    var licenseNumber: String? = null
    var vehicleValidType: String? = null
    var issuedDate: String? = null
    var expiryDate: String? = null
    var licenseThumbnail: String? = null

    //driver reference cities
    var refOne: String? = null
    var refTwo: String? = null
    var refThree: String? = null
    var refFour: String? = null
    var refFive: String? = null

    //driver offers cities
    var offerOne: String? = null
    var offerTwo: String? = null
    var offerThree: String? = null

    constructor() {

    }

    constructor(
        key: String?,
        uid: String?,
        name: String?,
        email: String?,
        password: String?,
        phone: String?,
        nic: String?,
        thumbnail: String?,
        userType: String?,
        brand: String?,
        model: String?,
        year: String?,
        color: String?,
        interiorColor: String?,
        vehicleThumbnail: String?,
        licenseNumber: String?,
        vehicleValidType: String?,
        issuedDate: String?,
        expiryDate: String?,
        licenseThumbnail: String?,
        refOne: String?,
        refTwo: String?,
        refThree: String?,
        refFour: String?,
        refFive: String?,
        offerOne: String?,
        offerTwo: String?,
        offerThree: String?
    ) {
        this.key = key
        this.uid = uid
        this.name = name
        this.email = email
        this.password = password
        this.phone = phone
        this.nic = nic
        this.thumbnail = thumbnail
        this.userType = userType
        this.brand = brand
        this.model = model
        this.year = year
        this.color = color
        this.interiorColor = interiorColor
        this.vehicleThumbnail = vehicleThumbnail
        this.licenseNumber = licenseNumber
        this.vehicleValidType = vehicleValidType
        this.issuedDate = issuedDate
        this.expiryDate = expiryDate
        this.licenseThumbnail = licenseThumbnail
        this.refOne = refOne
        this.refTwo = refTwo
        this.refThree = refThree
        this.refFour = refFour
        this.refFive = refFive
        this.offerOne = offerOne
        this.offerTwo = offerTwo
        this.offerThree = offerThree
    }


}
