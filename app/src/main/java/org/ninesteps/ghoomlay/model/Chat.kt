package org.ninesteps.ghoomlay.model

import java.io.Serializable

class Chat:Serializable{
    //sender
    var sname: String ?= null
    var sthumbnail: String ?= null
    var sid: String ?= null

    //receiver
    var rname: String ?= null
    var rthumbnail: String ?= null
    var rid: String ?= null

    var roomid: String ?= null
    constructor(){}
    constructor(
        sname: String?,
        sthumbnail: String?,
        sid: String?,
        rname: String?,
        rthumbnail: String?,
        rid: String?,
        roomid: String?
    ) {
        this.sname = sname
        this.sthumbnail = sthumbnail
        this.sid = sid
        this.rname = rname
        this.rthumbnail = rthumbnail
        this.rid = rid
        this.roomid = roomid
    }


}