package org.ninesteps.ghoomlay.model

import java.io.Serializable

class City: Serializable {
    var name: String ?= null
    var description:String ?= null
    var province: String ?= null
    var location: String ?= null
    var longitude: Double ?= null
    var latitude: Double ?= null
    var thumbnail1: String ?= null
//    var thumbnail2: String ?= null
//    var thumbnail3: String ?= null
//    var thumbnail4: String ?= null
//    var thumbnail5: String ?= null

    constructor(){

    }

    constructor(
        name: String?,
        description: String?,
        province: String?,
        location: String?,
        longitude: Double?,
        latitude: Double?,
        thumbnail1: String?
//        thumbnail2: String?,
//        thumbnail3: String?,
//        thumbnail4: String?,
//        thumbnail5: String?
    ) {
        this.name = name
        this.description = description
        this.province = province
        this.location = location
        this.longitude = longitude
        this.latitude = latitude
        this.thumbnail1 = thumbnail1
//        this.thumbnail2 = thumbnail2
//        this.thumbnail3 = thumbnail3
//        this.thumbnail4 = thumbnail4
//        this.thumbnail5 = thumbnail5
    }


}