package org.ninesteps.ghoomlay.model

import java.io.Serializable

class Map:Serializable{
    var name: String ?= null
    var longitude: Double ?= null
    var latitude: Double ?= null

    constructor(){

    }

    constructor(name: String?, longitude: Double?, latitude: Double?) {
        this.name = name
        this.longitude = longitude
        this.latitude = latitude
    }

}