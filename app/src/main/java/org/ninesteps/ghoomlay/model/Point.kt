package org.ninesteps.ghoomlay.model

import java.io.Serializable

class Point:Serializable {
    var name: String? = null
    var thumbnail: String? = null
    var type: String? = null
    var city: String? = null
    var refrence: String? = null
    var distance: String? = null
    var location: String? = null
    var longitude: Double? = null
    var latitude: Double? = null
    var opentime: String? = null
    var closetime: String? = null
    var phone: String? = null
    var tagone: String? = null
    var tagtwo: String? = null
    var tagthree: String? = null

    constructor() {

    }

    constructor(
        name: String?,
        thumbnail: String?,
        type: String?,
        city: String?,
        refrence: String?,
        distance: String?,
        location: String?,
        longitude: Double?,
        latitude: Double?,
        opentime: String?,
        closetime: String?,
        phone: String?,
        tagone: String?,
        tagtwo: String?,
        tagthree: String?
    ) {
        this.name = name
        this.thumbnail = thumbnail
        this.type = type
        this.city = city
        this.refrence = refrence
        this.distance = distance
        this.location = location
        this.longitude = longitude
        this.latitude = latitude
        this.opentime = opentime
        this.closetime = closetime
        this.phone = phone
        this.tagone = tagone
        this.tagtwo = tagtwo
        this.tagthree = tagthree
    }


}